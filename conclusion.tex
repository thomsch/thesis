\chapter{Conclusion}
\label{sec:conclusion}

\section{Summary}
Our vision is to help developers better understand the code artifacts they work with. With this research, we took
the first step by studying the fluctuations in quality metrics in projects' revision histories. By characterizing the
metric fluctuations we aim to better understand change, especially design, and to facilitate the detection of change
patterns that can be relevant as documentation to developers or other stakeholders.

Given the foundational nature of our research, we first presented an qualitative exploration of \jfreechart. Then, we
refine our comprehension of the domain in a second study where we conduct a quantitative study of the entire revision
history of thirteen projects that characterizes metric fluctuations categories and observing how they change in
different contexts. In a third step, we conduct a qualitative study on a sample of commits to understand if internal
quality metrics in the context of refactoring  are correlated with design intent from developers. Finally, having
tested our hypothesis and built a significant dataset, we built a model to detect applications of SOLID principles in
commits.

We observed that source code changes containing refactorings affects internal quality metrics and design in a number of
ways. Refactoring induced changes are more likely to improve or deteriorate the internal quality of a software than
commits without refactorings. Moreover, \glspl{rc} containing certain metric tradeoffs were found to be relevant
indicators for important design quality tradeoffs, likely embodying the design intent of the author (RQ1). Secondly, we
created a robust taxonomy to characterize metric fluctuations on two, but not limited to, dimensions enabling
researchers to navigate through vast number of commits and select relevant change patterns (RQ2). Thirdly, we found that
there is a dependency between metric fluctuations and development context. Metric fluctuations will change based on the
context, giving insight on the effect of a development activity like refactoring or the type of changes between test
code and business logic code (RQ3). Finally, we completed this first step by demonstrating that metric fluctuations can
give insight about a software's design by building a model using the fluctuations of 52 metrics to detect the
application of SOLID design principles with a \pct{68.8} class prediction accuracy and an average F1 score of
\pct{66.4} (RQ4). In conclusion, through a assortment of qualitative and quantitative exploratory studies, we were able
to demonstrate that metric fluctuations in \glspl{rc} can be used to gain insights about the design evolution of the
source code.

\section{Limitations \& Future Work}
% Discussion (Chapter 6) of Michalis' TSE paper (https://famelis.files.wordpress.com/2019/06/tse19-draft.pdf)
% Discussion (Chapter 6) of our 2019 CASCON paper

In this section, we discuss the next steps to be addressed in order to generate historical based design meta-data for
each artifact. We envision this meta-data to automatically document design concerns and facilitate code comprehension
tasks for developers, enabling them to make reliable and informed development decisions.

\subsection{Technical Improvements}
The most straightforward future work is to improve the data acquisition pipeline in making it easier to use and more
flexible for researchers.

% Project selection
For a study about design and refactoring, several factors can be important in selecting a project. Depending on the
research questions and what one is looking for, important factors may include programming language, architecture style
(object-oriented or other), history length (i.e., number of revisions or commits), number of developers and contributors
and others. Thankfully, most \gls{vcs} service providers, like GitHub~\cite{github}, Bitbucket~\cite{bitbucket},
GitLab~\cite{gitlab}, which contain a large number of open source software systems of great variety, already contain
this metadata for each project. Therefore, we can integrate a repository crawler like Sourcerer~\cite{linstead09} or
develop a customizable one, where we can specify the criteria and fetch the repositories to be analyzed.

% Support of other VCs
Despite the vast use of Git in software development nowadays, some projects, particularly older ones, are often hosted
on different \glspl{vcs} such as Subversion~\cite{svn}, Mercurial~\cite{mercurial}, or CVS~\cite{cvs}. We want to support these
systems in \metrichistory so that researchers have access to a larger pool of projects and enables them to see if
\glspl{vcs} have an impact on the development process.

% Seamless parallel computation
Another current limitation is the time it takes to extract the metric for each commit of a project, especially ones with
an large number of lines of codes or projects with a long version history. In our research, we used Akka~\cite{akka}
to distribute the workload on multiple computers. This setup, while very effective, requires extra time to arrange and
learning how the framework works. Optimally, we want a seamless implementation of distributed computing behind
\metrichistory where the researcher wouldn't have to think about it, saving him a lot of time in learning and
troubleshooting.

% Incremental metric computation
Staying in the domain of computation, we found that we could process a high number of commits in large projects if
instead of computing from scratch the metrics for each version, we base the metric computation on the previous version
plus the changes, effectively doing incremental static analysis between versions. This is not a straightforward task,
particularly for metrics measuring dependencies such as incoming and outgoing invocations.

% Database
Finally, metrics are currently produced and stored in \gls{csv} files. To accommodate a large volume of measurements,
\metrichistory can already split the data into one file of metrics per commit, avoiding the generation of very large
files (i.e., almost a hundred gigabytes in certain projects) and the subsequent problems faced when trying to work with
them such as limited RAM and parsing time. These concerns can entirely be avoided if we integrate a database as a core
part of our methodology and process. Measurements would directly be saved in the database and the post processing would
query the database for only what it needs. This solution would reduce storage needs of researcher and improve the speed
of saving new data and accessing existing data. Moreover, this configuration comes with a significant advantage: it
scales. Indeed, it would be straightforward to host the database in the cloud or in a server farm and add as many
resources necessary. This last benefit brings another one. It enables us to open our dataset to collaboration to other
researchers by providing the public address of the database instead of giving a static copy of our dataset. This way,
researchers can access the database for their own studies and even enrich the database.

\subsection{Methodology Improvements}
Regarding methodology, we identified multiple areas that can be improved. First of all, regarding metrics, we intend to
expand our analysis to include more quality metrics, as well as other types of static analysis (e.g., bugs,
anti-patterns, code smells). Additionally, we want to look at the effect of granularity on fluctuations measurements. In
these studies, we only look at the commit level. We believe interesting change patterns can emerge on software classes
or packages through time. Finally, we want to also include the relative changes in metrics between two versions.
Currently, we only use absolute values which we believe loses information.

The quality of the version history is a critical aspect for our method. By being confronted to a diversity
of version histories, we were able to confirm that the hygiene of the commits in a project has a direct impact on the
difficulty and time necessary to understand the project and source code changes. Specifically, we noticed that commits
containing tangled changes are problematic. These commits are harder to understand for researchers and developers alike.
Moreover, they also introduce non-negligible noise that we believe is detrimental to our approach. To solve this issue
we want to find a method to untangle unrelated sets of changes inside one commit leveraging existing work on tangled
commits~\cite{arima2018} and software traceability~\cite{spanoudakis2005software, Li2019}.

Another critical aspect is our aggregation process. Currently, we use a simple approach where we sum the changes of
metrics for each class for each metric. As discussed previously, this technique has several drawbacks. We want to refine
this aggregation process by working with distributions of changes for a given metrics instead of collapsing all the
change points into one value. This distribution would enable more complex analysis and use advanced statistical tools to
identify how that changes are distributed at the level of a commit.

Finally, we want to compare the decision tree model we used detect SOLID principles with other supervised learning
approaches such as linear models, support vector machines, stochastic gradient descent, nearest neighbors, Bayesian
approaches, and ensemble methods~\cite{friedman2001elements}.

\subsection{Indicators of design}
In future work, we want to investigate whether we can use alternative sources such as self-admitted technical debt, code
documentation, and bugs as indicators of design knowledge. Additionally, we want to encompass a larger set of design
changes and intent than a subset of architectural concerns. We want to include as indicators additional architectural
changes but also factors such as semantic changes, frameworks, and language choice. The rationale is that the process of
software development and thus designing software is never done in a vacuum but is tributary to the context in which its
developed. Certain programming languages and paradigms prefer certain code structures while others privilege something
else. This is also true for frameworks which often require particular ways of programming, forcing the developer to
write in a certain way. Semantic changes, such as renames, are also a powerful tool to understand the design choices
among of group of software artifacts since, in a world of abstract concepts, names and their relations form the cognitive
fabric of an application; they give meaning to the arrangements of code developers come up with to satisfy the
stakeholders requirements.

\subsection{Design Intents and Cross-referencing}
The manual identification of design intent was possibly the most complicated step of our process and the one that
required extensive manual effort. The reason for this is because refactorings, design decisions and generally the intent
of developers is not always explicitly expressed in comments or documentation. For \jfreechart, the artifact that
conveyed the most information was the changelog. The changelog was Javadoc comments that preceded one or more classes
within a commit (usually classes that had the most significant changes) and described in natural language how the
classes were changed. Conversely, commit comments did not contain much information about design intent or something more
high-level other than the change itself. In order to understand the intent, we studied the commit comments, the
changelog and the source code itself, and we went back in the project’s history to understand more about the changes and
the evolution of the metrics. The automation of this step would require significant effort. The first idea is to employ
\gls{nlp} to mine all textual data of the project (e.g., mails, commit comments, source code comments, changelogs) and
look for specific textual patterns pertaining to design and design maintenance. Next, we will use time-series clustering
to find relationships between current and previous changes to track design decisions through the project’s history.
Finally, we will use association rules to complete the cross-reference step between the classified commit according to
metrics and the design decisions as identified before. It is not certain that manual effort will be completely
eradicated even after the use of these machine learning techniques, but the goal is to minimize it as much as possible
and increase the accuracy of the analysis.


% Some commits have no SOLID principles but are completely fine depending on the task at hand. For example, a commit
% can be clarifying documentation or improving variable names. The SOLID flag wouldn't be raised but the commit is
% actually very good since it's improving the quality of the code.
