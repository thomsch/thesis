\chapter{Introduction}
\label{sec:introduction}

%
% As software development processes have matured, several best practices for software design have emerged and been
% established in the form of design patterns (or antipatterns as counter-examples) and design principles. Such patterns
% and principles are generally recognized by developers and designers and it can be said that, besides guaranteeing a
% certain level of design quality, they can also increase the understandability of the software design. When refactoring
% is used to explicitly introduced design patterns~\citn{kerievsky} or design principles, they can be perceived as
% conscious activities of design maintenance. As such, it can be argued that they carry clear design intent and they can
% be used to update documentation, improve communication and generally avoid design and documentation erosion.

% Therefore, the main argument of our work is that identifying changes that introduce design principles can bridge the
% knowledge gap about design at the interval between conception and after the release of the software. Especially for new
% commits, this knowledge can also guide and facilitate quality assurance processes including reviews and certification.
% In this work, we propose a classification method to detect the introduction of design principles by refactoring using
% changes in metrics between revisions as the measurable and observable impact of the change on design quality. By
% employing highly explainable classification methods, namely decision trees and random forests, our method also produces
% ``rules'' that can guide future maintenance activities.
%

Software development is a complex activity. It requires developers to take into account different perspectives and often
switch contexts that are costly to interrupt \cite{fritz}. Since the inception of the field, concerted efforts from
research, industry, and hobbyists have been made to improve the development experience by seeking to ease out pains and
enable developers to make less mistakes, be more productive, be automatically warned about bugs, improve organization,
communicate more efficiently, and ensure that existing features do not deteriorate. Innovations such as automated
testing and linting have significantly contributed to not only improve program quality, but also decrease some of the
load borne by developers by automating these menial tasks. This arrangement can reap benefits throughout the entire
lifecycle of applications: developing is facilitated, enabling developers to create better products for the user and for
themselves when they do maintenance down the line. Automated testing ensures that the features of an application do not
deteriorate when changes are made to the source code and notify developers when they introduce breaking changes. It is
typically implemented with a systematic approach, where developers specify the expected behaviors for each component or
subsystem. Automated linting enforce coding conventions and by reformatting the source code accordingly, guaranteeing a
common code base between developers that is easier to read.

\section{Modern software development}
Nowadays, the software development workflow has evolved to be more agile where a strong emphasis on the creation of
working implementation increments is placed in opposition to earlier software development workflows, such as the
waterfall model, favoring a detailed up-front design. These models are seldom used nowadays for a reason: their
monolithic structures were not able to scale in terms of turnaround and complexity.

On the one hand, this allows flexibility and rapid development cycles, leaving patches, corrections, and enhancements to
be applied post-release. On the other hand, this practice tends to accumulate technical debt~\cite{kruchten12} and
requires a lot of maintenance effort to continue development, leading to a deterioration in non-functional
requirements such as design quality (e.g., maintainability, extendibility, understandability).

Although still an integral part of software development with recognized value, careful and detailed design is often
neglected in these iterative workflows and is usually postponed for a later time, often after the system has been
released. Given the rapidity of development cycles, this inevitably leads to design erosion~\cite{vangurp02}, where
assumptions held originally by designers are no longer valid, and design evaporation~\cite{robillard16}, where knowledge
about design is lost.

% In development settings that focus on implementation with minimal up-front design, refactoring is interleaved
% with the implementation of functionality and enables continuous adaptation of design to new requirements.

These methodologies have recognized this shortcoming, and they recommend the extensive use of refactoring~\cite{opdyke92}
-- typically small, local changes that improve design quality without affecting the system's observable behavior --
which can be characterized as post-release changes (i.e., maintenance) to the system's structure. They aim to prepare
the source code for future extensions and functional enhancements, a process known as "preventive
maintenance"~\cite{chapin01}. However, even if the design can be corrected, refactoring activities may not be explicitly
recorded, and changes in design are not always reflected in the available documentation at hand. This can significantly
reduce the developers' awareness and knowledge of the system, which in turn can hinder several tasks, including
onboarding of new developers and communication between stakeholders.

These contemporary software development practices are enabled by the adoption of \glspl{vcs}. These systems archive code
artifacts and integrate themselves into a vast range of development workflows. Their position as a core element of
contemporary software development processes is not only due to their low barrier of entry and ease of use. Indeed, their
main advantage lies somewhere else. They enable developers to look at and interact with the development history of a
project. On top of gaining insights about past changes and reverting them if needed, they bolster the collaboration
capabilities of developers, given that software products are often too big and complex for a sole developer, and
simultaneous collaborative development is essential.

\section{Problem Statement} % Motivation, problem

In this fast-paced and collaborative environment, developers are called to make changes in any part of the source code
which they may not be familiar with, either due to a long absence from this part or that code was modified or added by
another collaborator. Thus, to make valuable contributions to the code base, developers must first comprehend
the portions of the source code related to their task. They peruse through multiple sources of information, such as code,
wikis, discussions, commit messages, and others, often having to sift through potentially out of date
information~\cite{robillard16}. They spend a substantial amount of their time on comprehension to be able to best
maintain, add new functionalities, and remove bugs~\cite{lientz78}. Ultimately, successfully contributing to a project
requires contextual design knowledge, which is typically accumulated tacitly overtime throughout the development history
of the project.

Unfortunately, extracting relevant and pertinent information about an artifact from its commit history is a daunting
task. It involves relying on commit messages and descriptions of the revisions, which -- when they exist --
can be irrelevant or unhelpful in describing the changes. The only other alternative is to examine what is commonly
known as the "diff" -- the list of source code changes between commits for each file generated automatically
obtained from the \gls{vcs} -- but this process is confusing and slow~\cite{Tao2012} especially since developers are not
interested in the comprehensive knowledge offered by the entire revision history of a component: they need particular
information about the most relevant changes for their current task or recent changes.

% \opt{Adding a multi-figure illustrating 1) the multitude of commits in a history; 2) an example of diff; 3) a list of changed files }

\begin{figure}[b]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/changes-highlights}
    \caption{Commits containing design changes are highlighted compared to the rest of the version history. Each commit
    also details which software artifacts had their design changed.}
    \label{fig:changes-highlight}
\end{figure}

Thus, we aim to find a solution that enables developers to quickly get informed about the design history of the source
code that is relevant to their current task. We want to focus on acquiring design information disseminated through the
evolution of the code. Current alternatives are lackluster because they are either based on reading documentation, which
is often not up to date and thus not a reliable source of information, or they are requiring developers to sift through
heaps of code changes for multiple files, which is error-prone and very time-consuming. We sketch the concept in
Fig.~\ref{fig:changes-highlight} where specific commits relevant to design are highlighted from the other changes
registered in the version history.

\section{Research Questions}
\label{sec:research-questions}
% We follow the guidelines established by Easterbrook et al.~\cite{Easterbrook2008, easterbrook2014course}. We choose a
% post-positivist philosophical stance. It defines knowledge as being objective and that outcomes of phenomena are
% determined by causes. We use a quantitative approach to isolate complex things into manageable sizes and derive
% statistical generalizations over our population of observations. We also use a qualitative approach to label our
% observations and build a theory. Finally, our approach leverage a mix of exploratory and causal methods. At the core of
% our mix-method approach, we use archive analysis to construct our initial database of observations, followed by a case
% study, benchmarks and rational reconstructions.

We define the goal, purpose, quality focus, perspective, and context of our study according to the
guidelines defined by Wohlin et al.~\cite{wohlin2012}. The \emph{goal} of our study is to analyze metric fluctuations
brought by commits for the \emph{purpose} of evaluating the relationship between refactoring, fluctuation of internal
quality metrics, and design intent. The \emph{quality focus} is on the effectiveness of employing refactoring and metric
fluctuation data for the identification of commits that involve SOLID design principles which are crucial for shaping
system design. Study results are interpreted from the \emph{perspective} of researchers with interest in the area of
software design and development processes as well as programming practitioners. The results are of interest to
developers that need to understand the design of a project through studying important milestones in its evolution and by
software architects seeking to review design decisions in committed code in order to confirm their architectural
conformance. The \emph{context} of this study comprises change and issue management repositories of a set of open source
projects.

We articulate a series of empirical studies with the following \glspl{rq}:
\begin{enumerate}
\item How does refactoring impact internal quality metrics and design?
\item Can we classify the changes in internal quality metrics?
\item What is the effect of environmental conditions to metric fluctuations?
\item Can fluctuations in metrics be an indicator of the application of SOLID principles?
\end{enumerate}

% To answer them, we studied the development history and refactoring activity of 11 software projects. First, we used
% \rminer~\cite{rminer18} to retrieve the commits in the revision history of each project that contained refactoring
% operations. Then, we calculated internal quality metrics for the versions before and after such \glspl{rc}
% using \sourcemeter~\cite{sourceMeter}.

% To answer RQ1, we studied the fluctuations in metrics for these revisions, finding that refactoring activities often
% lead to \emph{tradeoffs} in metrics. To answer RQ2, we manually annotated a sample of all \glspl{rc}, finding
% that, on average, \pct{67} of \glspl{rc} involving tradeoffs also carry design intent. To answer RQ3, we
% studied in depth all \glspl{rc} involving tradeoffs for two projects, finding that the design rationale of
% \pct{79} of them can be attributed to conscious applications of known design principles. These observations allowed us
% to draw conclusions about the relationship between refactoring, tradeoffs and design intent. Notably, we find that
% fluctuations to a handful of internal quality metrics can be reliably used to find commits in a project's version
% history that carry design intent.

\section{Approach} % Motivation, solution

% However, despite this well proven relationship between refactoring and design, to the best of our knowledge there is no
% work that uses refactoring as an indicator for detecting the presence of design decisions. It is generally unsurprising
% when refactoring improves the quality of software monotonically. This is after all the intended purpose of refactoring.
% Things are more interesting when refactoring coincides with \emph{design tradeoffs}, i.e., decisions about design that
% are non-monotonic with respect to quality, i.e., improve some quality characteristic at the expense of others. Since
% refactoring is an activity in which developers embark intentionally, it is reasonable to conjecture that the
% co-occurrence of refactoring and design tradeoffs indicates a potentially pivotal point with respect to the design of a
% software artifact. The ability to recover such pivotal points in time is very useful. For example, developers can use
% this knowledge to guide their decisions during software evolution. Further, recovering such design tradeoffs can help
% mitigate design erosion and evaporation as source code is the most reliable and immutable information about a system
% since it changes by nature with it.

"\textit{Design}" is a notion intuitively understood by humans and developers alike. However, the concept englobes several
layers and components in the context of software development. While our end goal is to provide information about design
changes in general, we have to scope out a reasonable target for this exploratory research. Thus, we choose to use design
principles as the embodiment of design changes. Design principles are sets of well-established guidelines used by developers
to guide them in the practice of agile design~\cite{Martin06}. They help identifying and removing design smells that impact
the flexibility, reusability and maintainability of software systems.

To achieve this, we first need an intuition about what can characterize these changes in design. We know that
refactoring is used to change non-functional aspects of software to facilitate future extensions and reverse design
erosion and that we can measure source code to approximate internal qualities of a software~\cite{softwareMeasurement}.

Thus, we begin by exploring commits containing refactorings, which we call \glspl{rc}. Refactoring has been extensively
studied for its impact on design quality~\cite{AlDallal18,stroggylos07}, and influence on developer habits with respect
to its application on software systems~\cite{tsantalis13,Silva16}. Such studies have focused primarily on the
identification of refactorings and refactoring opportunities and on analyzing their impact on design, in terms of the
presence of code smells~\cite{gueheneuc2007,tsantalis09} or the fluctuation of code metrics~\cite{marinescu04}. The
resulting consensus is that refactoring impacts the design of a system in a significant way. This relationship is not
incidental: developers purposefully use refactoring to express specific design intentions~\cite{Silva16} and use
recommender systems to identify the most suitable refactorings to best suit their intents~\cite{Bavota2014}. Refactoring
activity can carry various kinds of design intent, including but not limited to: removal of code smells, resolution of
technical debt, the introduction of design patterns, and application of design principles.

We propose to use fluctuations in metrics brought by the code changes from one commit to another to classify different
kinds of refactoring activity. \emph{We assume that when a \gls{rc} affects multiple metrics, improving some while
deteriorating others, we have evidence of a developer intentionally changing the design (design intent) by making
specific tradeoffs.}

If this assumption holds, we have a good prior to use fluctuations in metrics and their tradeoffs as indicators of
design related changes. From there, we have a sound rationale to make an experiment where we try to predict the presence
of the application of design principles in commits using metric fluctuations.

To test the assumption, we conduct an initial study on \jfreechart, a library for displaying graphical charts in
applications~\cite{gilbert2002jfreechart}, using a qualitative analysis. We look at the correlation between a selection
of internal quality metrics and the design changes introduced in \glspl{rc}. Furthermore, we dive deeper into the effect
of internal quality metrics changes on different development contexts in a quantitative analysis and we also refine our
comprehension of the relation between refactoring and design changes, specifically changes susceptible to embody a
design decision, in a third preliminary study. Once the hypothesis is tested, we train a general model on a dataset of
annotated commits using supervised learning and evaluate the capacity of the model to recognize the application of the
SOLID design principles, a set of design principles commonly used in \gls{oop}, in commits from new projects (projects
that were never seen during training).

Our approach is based on existing theories such as internal quality metric measurement~\cite{sourceMeter} and quality
characteristic appreciation~\cite{bansiya2002hierarchical}, and we follow the open source principle of considering the
code as the most authoritative source of design information~\cite{gacek2004many}. We use a mixed-method approach,
consisting of several exploratory case-driven archive analysis, and comparative explorations. We envision our work as
complementary to other approaches for extracting tacit and contextual design knowledge such as from
discussions~\cite{viviani18}, and commit messages~\cite{da2017using}.

% In practical terms, we need a better understanding of how developers use refactorings not just as quick fixes, but as a
% tool to introduce larger scale design and architectural decisions. We conjecture that a first indicator can be the
% design tradeoffs that are made during refactoring. Refactorings are intended to improve software quality and should thus
% improve particular design quality metrics. However, this is not monotonic for all metrics; a refactoring may cause some
% metrics to improve, while others to deteriorate. Can such fluctuations be used to detect design tradeoffs?

\section{Potential future benefits}
% \opt{Detect architecture leader.}
In this section, we present the multiple hypothetical benefits of our proposal.

Our proposal could provide developers with an approach that enables them to filter out code changes that are not relevant
to design. They would be able to quickly see the relevant last commits affecting the design and understand why it was
changed, e.g., a response to the introduction of a new feature or a bug fix. This comprehension allows them to
contribute relevant code changes that make use of the existing design efficiently. Moreover, when applied to an
\gls{ide}, we could imagine a dedicated pane that would provide developers an overview of the moments in time when the
design changed for the software artifact (e.g., a file, a class, a model) they are currently editing, helping the
developer in his exploration and comprehension of the artifact, and allowing him to quickly go back in time or get more
information if he interacts with one of the moments in time displayed.

% \begin{figure}[b]
%     \centering
%     \includegraphics[width=0.8\textwidth]{figures/ide-integration}
%     \caption{Illustration of our approach integrated in a developer workflow}
%     \label{fig:integrated-design-history}
% \end{figure}

This approach could also be applied to the code reviewing process where developers are assigned changes made by other
developers to review~\cite{codeReview}. This practice is a staple in major software companies~\cite{codeReview}. It
allows to catch bugs early and fosters a shared knowledge of the system among its developers. By integrating our
approach into the review process, we could detect when a set of changes is susceptible to contain design changes and
warn the developers that an increased attention should be given for these code changes. Reviewers would be able to see
the modifications to the design and conduct sanity checks to make sure that changing the design is the best solution.
Moreover, this application provides a good opportunity to include a feedback mechanism that would help our system to
improve its predictions as reviewers use it.

Another benefit would take place during the onboarding process where new developers get acquainted with the software
systems. Our approach would help them identify key moments in the construction of the software, informing them about the
motivations behind the present architecture and state of software which they wouldn't have otherwise with their minimal
experience.

Ultimately, we want to generate a descriptive, history-based meta data for each code artifact that could be used by
developer and other stakeholders to synthesize documentation, and to empirically assess software and software projects
and make informed decision. With this research, we take the first steps towards this direction.

\section{Contributions}
We make the following contributions:
\begin{enumerate}
    \item A systematic methodology to mine internal quality metric fluctuations from \gls{vcs}.
    \item An open source toolchain that implements it, called \metrichistory~\cite{metricHistory}.
    \item A deep qualitative analysis of the revisions containing refactorings in one open source project, \jfreechart.
    % \item A deep qualitative study of the refactoring history of 2 software projects.
    \item A quantitative study of the metric fluctuations in 13 open source projects.
    \item A scheme for classifying metric fluctuations.
    \item A public data repository of historical internal quality metric fluctuations.
    \item A manually annotated dataset composed of 928 commits identifying the presence of SOLID principles.
    \item A procedure to build and train a classifier to detect SOLID principles in version histories and its empirical
    validation.
    % \item The evaluation of a classifier generated by this procedure.
    \item A general model that can identify the application of the SOLID principles in commits.
    % \item A procedure to conduct a user study to evaluate the sentiment of developers towards the results of the
    % classifier.
    \item The outline of an approach for filtering a project's revision history to a
    set of revisions that have a high likelihood to carry design intent.
\end{enumerate}

\section{Structure of the thesis}
The thesis is organized as follows. Chapter~\ref{sec:background} explains the core background concepts. In
Chapter~\ref{sec:datacol} we illustrate the overall design of the studies as well as the processes used to gather the
various data used throughout our research. Then, in Chapter~\ref{sec:preliminary} and Chapter~\ref{sec:solid} we
describe the studies performed to test our assumption and answer the \glspl{rq}. We present the related work in
Chapter~\ref{sec:relatedwork} and summarize our findings and discuss future work in Chapter~\ref{sec:conclusion}.

% \opt{If you want to read this go to $X$}

\section{Work attribution}
The research presented in this thesis is an international study project with Prof. Michalis Famelis, Prof. Marios
Fokaefs and Dr. Vassilis Zafeiris. Particularly, Dr. Zafeiris collaborated with me in building the annotated datasets
(Section~\ref{sec:explore-jfreechart},~\ref{sec:prelim-design-intent},~\ref{sec:oracle}), distributing mining workload
across different computers (Chapter~\ref{sec:datacol}), analyzing the effect of releases on metric tradeoffs
(Section~\ref{sec:context-releases}).
