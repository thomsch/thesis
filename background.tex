\chapter{Background}
\label{sec:background}

\section{Software Design}
Software design is the process following the requirements collection and preceding the implementation of a system or its
components~\cite{design2010}. The purpose of this activity is to create a structure of the program, similarly to an
architect drawing the plans for a new building, that satisfies the requirements established previously. However, the
designer also has to balance other considerations such as extensibility, modularity, and maintainability that will
affect the software's quality in the long term. Balancing the expectations of multiples stakeholders is difficult and it
is impossible to satisfy everybody. To mitigate the risks and focus on the right design areas, developers use design
principles such as SOLID principles in \gls{oop} and design patterns as heuristics to guide them~\cite{booch2006object,
Martin06}.

\section{SOLID Principles}
\label{sec:background-solid}
The SOLID principles are a set of well known design principles in the oriented object community. It is composed of five
principles: \gls{srp}, \gls{ocp}, \gls{lsp}, \gls{isp}, \gls{dip}~\cite{martin2000design}. The SOLID principles are
meant to guide the practice of agile design by helping to identify and remove design smells that impact the flexibility,
reusability and maintainability of system design. In theory, the implementation of decisions based on SOLID principles
is performed with manual or automated refactorings.

\gls{srp} specifies that a software artifact such as a class, method or package should only have one responsibility, or
in other words, only one reason to change. This principle encourages the separation of concerns which contributes to
increasing the cohesion of artifacts and decreasing their coupling with other elements. For example, if a class
handles the creation and logging in of users, an application of \gls{srp} could lead to split the class in two. One
class would now have the responsibility of creating users and the other one would have the responsibility of logging
them into the system. This way, if the creation procedure changes, the class handling logins will be left untouched,
reducing the chance to induce bugs and facilitating the work of developer.

\gls{ocp}, credited to Bertrand Meyer~\cite{Meyer97}, specifies that software artifacts should be open for extension but
closed for modification. The idea is to reuse existing components by extending them to new needs rather than modifying
them directly. The advantage of this concept is to avoid problems stemming from modifying artifacts that are used by
multiple components inside a system. If you modify the original artifact it will change the behaviors of the dependents
in unforeseeable ways which often lead to regression problems and backward incompatibilities which are hard to solve. By
extending the artifact's features, you do not touch the original behavior. A very popular example is the use of
polymorphism in object oriented languages such as Java. The developers can change the behavior of a base class by
implementing a subclass. For example, you can extend a class representing a collection of heterogeneous elements through
a subclass to implement the concept of a mathematical set. The newly created subclass is still representing a collection
but it is now enforcing particular semantics.

\gls{lsp} is a concept introduced by Barbara Liskov~\cite{Liskov1994}. In the context of type theory in \gls{oop}, it
specifies, that for a type $T$ and a subtype $S$, the objects of type $T$ can be replaced by objects of type $S$ without
changing the behavior of the systems where $T$ was expected. This principle can be applied to several situations in
practice such as mixed instances cohesion problems~\cite{pageJones}. This problem arises when a class's feature is
provided in at least a couple of implementations in its instances. The consequence is that the instances will have some
attributes or methods that are undefined or have unexpected secondary effects. By applying \gls{lsp}, the class will be
transformed into a base class $T$ and each implementation is sent to a subtype $S$, ensuring a consistent behaviour for
the feature provided by the class.

\gls{isp} specifies that is it better to have multiple interfaces, each dedicated to one aspect of a feature or concept,
rather than one big interface between two clients. This principle also promotes a separation of concerns and aims to
improve cohesion. As an example, we will model a modern printer. Modern printers have multiple capabilities such as
scanning, printing, and sometimes faxing. It can be tempting to create one interface regrouping all
these functionalities as an "\textit{All in one Printer}" and implementing our "\textit{Modern Printer}" after it. Now,
you reuse the interface to implement a simple machine that only prints. It leaves you with two features from the
interface your "\textit{Simple Printer}" can't provide and you have to code work-arounds to handle these impossible
cases. By using \gls{isp}, you can handle this situation elegantly by separating the \textit{All in one Printer}
interface in three: "\textit{Printer}", "\textit{Scanner}", "\textit{Fax}" and adapting \textit{Modern Printer} and
\textit{Simple Printer} in consequence. The former will implement all three interfaces while the latter will only need
to implement \textit{printer}.

\gls{dip} encourages developers to build components in such a way that they depend on abstractions rather than
implementations. More specifically, high level components should not depend on lower level components. The consequence
entails that abstractions will not depend on details, guaranteeing the generalizability, but details depend on
abstractions, therefore inverting the traditional dependency relationship where the main component of a system would
depend on detailed components. Developers usually implement this principle by adding a layer of abstraction between
dependencies. For example, in a system where the class \textit{Printer} depends on a class "\textit{Paper Tray}" as
shown in Fig.~\ref{fig:dip} a), we would abstract \textit{Paper Tray} behind an interface such as "\textit{Paper
Supplier}". This way, the higher level component, \textit{Printer}, would only about \textit{Paper Supplier} and not the
lower level component \textit{Paper Tray} as illustrated in Fig~\ref{fig:dip} b). When the components are grouped in
different modules, it is possible to push the principle even further by reorganizing the location of the abstraction. In
our example, \textit{Printer} is in a module "\textit{machine}" and \textit{Paper Tray}, as well as \textit{Paper
Supplier} are in a module "\textit{supplies}". This is quite a natural way to group elements as they belong the same
concept. However, this layout introduces a dependency between the \textit{machine} and \textit{supplies} because
\textit{Printer} depends on \textit{Paper Supplier}, which contradict \gls{dip} as we can consider that the former still
represents a higher level than the former. We solve this problem by moving the \textit{Paper Supplier} interface to the
module \textit{machine} as shown in Fig.~\ref{fig:dip} c). Now dependencies are fully inversed as the lower level
component, \textit{Paper Tray}, depends on a the higher level concept, \textit{Paper Supplier} as it is now close to the
class \textit{Printer} and also its only dependency.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/dip}
    \caption{Illustration of the application of \gls{dip} on a toy example. The colored arrow represents the dependency between a high component (left) and a lower level component (right). Note how the arrow changes direction from layout b) to c).}
    \label{fig:dip}
\end{figure}

\section{Version Control Systems}
\label{sec:background-tangledchanges}
\gls{vcs} are software systems that record changes to a set of files over time in an automated fashion. Originally, most
developers would copy and date the files they wanted to "save" into another folder on their computer. However, this
technique doesn't scale well and is not practical when collaborating with other developers. \gls{vcs} solves these
issues by providing developers an easy way to store changes and share their code with other colleagues that can also
propose changes into a ledger, commonly referred to as the "\textit{changelog}", where all changes are recorded.
Developers also gain the capacity to go back to any change they made and branch out from there, building an alternate
version of the software.

Moreover, a software project under version control is referred to as a "\textit{repository}". A new entry to the
changelog, in the form of a set of changes in the source files, is called a "\textit{commit}" or a "\textit{revision}"
depending on the terminology used by each \gls{vcs}. A "\textit{tangled}" commit denotes instances where multiple
unrelated sets of changes are cohabiting in the same commit, although the changes could be partitioned in different
commits~\cite{herzig13, Sothornprapakorn18}. On the opposite, clean or tangle-free commits, have only one set of
auto-contained, cohesive changes.

Some \glspl{vcs} provide distributed versioning, enabling developers to commit changes locally on their system and then
share their changes to a remote repository accessible by others. Examples of known and popular \glspl{vcs} include
\emph{Git}~\cite{git}, \emph{Subversion}~\cite{svn}, and \emph{Mercurial}~\cite{mercurial}.

\section{Mining Software Repositories}
Mining of Software Repositories is a method of archival research where researchers look at the evolution of the
artifacts in the project through time~\cite{msrWeb}. The adoption of Open Source and \gls{vcs} enables researchers in
software engineering to study the evolution of a software project in terms of specifications, source code, social
interactions between developers, and bugs from its inception to present days. To study these, they use the process of
mining, which is the systematic extraction of information relevant to the subject of study in the version history of a
project~\cite{kagdi2007survey}. For example, it is common to examine the changes in files or the \gls{vcs} meta-data.
Recently, the community has done substantial work on software defects and studying the dynamics of Open Source
collaboration~\cite{de2016systematic}.

Historically, this technique is based on early ideas of Ball et al.~\cite{ball1997}, where they make the argument that
\glspl{vcs} contains a lot of useful contextual information by exploring the evolution of class relationships through
time. Shortly after, an approach was implemented by leveraging the changes in between versions characterized by software
releases with ad-hoc toolchains to detect logical coupling~\cite{gall1998}. Then, later studies introduced experiments
using a smaller granularity of versions such as commit to commit. The early work of Zimmerman et al. is an example where
they leveraged \gls{vcs} information to detect files commonly changed together and propose
recommendations~\cite{zimmerman2004}. More recently, initiatives such as GitHubTorrent~\cite{Gousios2012GHTorrentGD}
democratized this practice by offering convenient facilities for researchers to select and retrieve massive amounts of
\gls{vcs} meta-data from version control systems.

Compared to them, we focus our efforts on extracting software metrics and their fluctuations instead of analyzing the
\gls{vcs} meta-data or file-based changes. Another difference is that our approach works at scale with the combination
of our versatile tool and its integration to a distributed computing system with Akka, a framework to build powerful
reactive, concurrent and distributed applications more easily~\cite{akka}.

However, software repository mining is not an approach suitable for every type of study. Especially when working with
public repositories, which are known to be often personal projects, or sometimes are not even related to software
development but used as a data storage service. Moreover, even legitimate software projects can be problematic since
their development workflows are sometimes coupled with private tools such as bug trackers or project management systems.
These tools contain valuable information about the development and history of a project, but they are often inaccessible
to repository miners. Thus, when mining software repositories, researchers have to be careful about the provenance and
the relevance of the repositories, and their data with respect to the objective of their
research\cite{kalliamvakou2014promises, cosentino2016findings}.

\section{Metrics}
We compute the metrics to measure the changes in the projects using \sourcemeter, a static analyzer that supports a wide
variety of metrics and granularities (e.g., method, class, package)~\cite{sourceMeter}. For this study, we focus on
class metrics. \sourcemeter offers 52 metrics for classes, distributed in 6 categories: \textit{cohesion metrics (1),
complexity metrics (3), coupling metrics (5), documentation metrics (8), inheritance metrics (5), size metrics (30)}
with the respective number of metrics for each category. The detailed documentation for each metric and how it's
calculated is available on \sourcemeter's publisher website \cite{sourceMeterDoc}.

In Chapter~\ref{sec:preliminary}, we use a subset consisting of four metrics: \gls{cbo} to measure coupling, \gls{dit}
for inheritance complexity, \gls{lcom5} for cohesion, and \gls{wmc} for method complexity. These metrics were selected
in a process explained in the aforementioned chapter.

\gls{cbo} measures coupling between objects by counting the number of classes a class is depending on. A high count
indicates that the class is linked to many others which can cause maintainability problems such as a brittle reliability
when the dependencies change often. \gls{dit} measures inheritance complexity by counting recursively the number of
subclasses a class has. In other words, it measures the length of the path to the deepest ancestor. The deeper the
inheritance tree runs, the harder it is to modify or reuse the elements of the hierarchy because they become very
specialized and depend on their parents for functionalities. \gls{lcom5} measures class cohesiveness by counting the
number of methods linked to the same attribute or abstract method as \gls{oop} practices advise that a class should only
have one responsibility. If multiple methods do not share the same attributes or abstract methods, it creates groups of
methods that are indicators of a fragmented class. The higher the lack of cohesion is, the lowest the responsibility of
the class is crisply defined, often resulting in brittle implementations of features that do not actually belong
together. Finally \gls{wmc}, measures the complexity, in terms of independent control flow path, for a class. It is
calculated by summing up the McCabe’s Cyclomatic Complexity~\cite{mccabe1976complexity} for each method and the
\texttt{init} block of a class. The higher the number, the harder it is to understand, and thus verify and maintain,
the class and its methods because of the number of different control flow paths available.

\section{Summary}
In this section, we introduced the core concepts we rely on in our research. Software design represents the end
phenomenon we are trying to capture and recover. However, the notion of software design, while well-defined, can be
somewhat abstract in practice. Thus, we use the SOLID design principles as a concrete and measurable manifestation of
software design concerns. In addition, we rely on \glspl{vcs} as the framework that allow us to retrieve the evolution
information of a software project as its history of change is recorded through it in the form of commits. Our study is
guided by the principles and methodologies developed for research in the area of Mining Software Repositories. Finally,
we introduce the metrics we use to characterize the source code changes happening between versions.

% commits are the smallest increment of change containing one or more set of source code changes at once.
