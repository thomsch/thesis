\chapter{Predicting SOLID Principles}
\label{sec:solid}

In this chapter, we aim to determine if metric fluctuations can be used as an indicator for detecting design changes (RQ4).
Specifically, using the knowledge acquired in the exploratory studies from Chapter~\ref{sec:preliminary}, we want to use
metric fluctuations to detect the applications of a known set of design principles, namely the SOLID design principles
(see Section~\ref{sec:background-solid}).

To answer this question, we create a predictive model and measure its capacity to predict if a commit contains the
application of a design principle and compare it to the ground truth. The ground truth is determined by an oracle that
we created by manually analyzing commits and annotating whether they contained applications of SOLID principles.

\section{Building an oracle}
\label{sec:oracle}
We annotate a sample of commits from the projects presented in Chapter~\ref{sec:datacol}. We sampled \glspl{rc} from the
entire project commit history of each project. The sampling size for each project is determined using a \pct{95}
confidence interval and a \pct{10} margin of error~\cite{samplingsize}. We tagged each \gls{rc} to indicate which of the
five design principles were detected, and if the commit is tangled. Additionally, we assign the project an overall
"commit hygiene" score to qualify the ease of readability and understandability of the commit messages and source code
changes.

% \opt{Examples of detecting a design principle}
% Commit "57edb21a2a7b53ed9d996c9b7148f7d3bc7fc438" from \okhttp is a good example.

\subsection{Protocol}
To annotate the set, we adapted the protocol of \ref{sec:explore-jfreechart} for \gls{rc} with SOLID principles and was
performed by the same two researchers. The consolidation phase, in this case, involved the establishment of heuristics
for deciding which SOLID principle(s) influenced design changes in each \gls{rc}. For instance, the redistribution of
class members through \MoveMethod, \MoveAttribute, \PullUpMethod refactorings that improve class cohesion are typical
applications of SRP. A typical indication for the application of OCP is the replacement of class dependencies on
concrete implementations with abstract classes or interfaces. The introduction of \TemplateMethod and \Strategy design
patterns is another hint for the application of OCP. Moreover, splitting a crowded\footnote{Also known sometimes as a
"fat" interface in the terminology used by M. Fowler~\cite{fowlerrefactoring}} interface to simpler ones denotes the
application of ISP. Finally, since both OCP and DIP require classes to \emph{depend on abstractions}, we distinguished
the application of DIP on the basis of ``ownership inversion'' of interfaces. Ownership inversion requires that an
interface is packaged in the same module with the client component that uses it. On the other hand, interface
implementations are packaged in external modules that depend on the client module~\cite{Martin06}. Regarding the
application of LSP, we searched for cases where the design of subclasses changes for conformance to the contract of the
superclass, e.g., narrowing down their public interface to match that of their parent, fixing pre/postcondition
violations in concrete overriding. A transcription of the complete guidelines is available in
Appendix~\ref{sec:appendix-solid}.

In order to gain more insight about the nature of the commits we were evaluating, we annotated two additional aspects.
First, for each commit, we flag to '1' if the commit is tangled and '0' if the commit isn't tangled (See
Section~\ref{sec:background-tangledchanges}). Second, at the end of a project's analysis, we also assign it an overall
"commit hygiene" score going from \emph{Poor}, \emph{Fair}, \emph{Good}, \emph{High}. Commits dedicated to a unique
modification in the source code (e.g., adding a feature, fixing typos, refactoring an aspect of a class) and showcasing
a clear message are signs of high commit hygiene~\cite{commitMessage} while commits bundling many changes together or
described vaguely are signs of low commit hygiene because it is hard to understand what changed in the commit and
defeats parts of the \glspl{vcs}' purpose.

\subsection{Dataset}

\begin{table}[ht!]
    \centering
    \caption{Summary of the collected dataset per project.}
    \begin{tabular}{l|c|lc|ccccc}
    Project & Sample size & Hygiene & Tangled & SRP & OCP & LSP & ISP & DIP \\ \hline \hline
    \ant & 91 & \textbf{High} & \pct{25} & 26 & 9 & 8 & 3 & 2 \\
    \argouml & \textbf{92} & Good & \pct{42} & 26 & 9 & 5 & 2 & 3 \\
    \daggertwo & 83 & Good & \pct{16} & 26 & 7 & 4 & 1 & 1 \\
    \hibernate & 91 & Fair & \pct{21} & 21 & 15 & 3 & 2 & 1 \\
    \jedit & 90 & \textbf{Poor} & \pct{\textbf{70}} & 41 & 14 & 7 & 4 & 4 \\
    \jena & 89 & Good & \pct{33} & 21 & 10 & 1 & 0 & 1 \\
    \jfreechart & 60 & Good & \pct{40} & 13 & 5 & 1 & 0 & 0 \\
    \jmeter & 90 & Fair & \pct{32} & 19 & 5 & 2 & 0 & 0 \\
    \junit & 75 & Fair & \pct{56} & 29 & 8 & 4 & 0 & 4 \\
    \okhttp & 80 & Good & \pct{33} & 30 & 9 & 4 & 0 & 1 \\
    \retrofit & \textbf{64} & Good & \pct{\textbf{9}} & 19 & 8 & 2 & 1 & 5 \\
    \rxjava & 83 & Good & \pct{28} & 19 & 7 & 5 & 2 & 1 \\
    \end{tabular}
    \label{tab:sample-size}
\end{table}

Overall, we manually analyzed a total 928 commits from 11 projects: \ant, \argouml, \daggertwo, \hibernate, \jedit,
\jena, \jmeter, \junit, \okhttp, \retrofit, \rxjava. The analysis was done by two reviewers independently after a pilot
run on \jfreechart used to synchronize the protocol and clear misunderstandings. The number of \gls{rc} for each
project, its associated commit hygiene and percentage of tangled commits, are presented in Tab.~\ref{tab:sample-size}
as well as the number of occurrences for each design principle.

Among all projects, we found 334 SOLID commits and 594 NON-SOLID commits. It's a ratio of 1:1.78 for SOLID over
NON-SOLID commits. This result is reasonable since we established in Chapter~\ref{sec:explore-tradeoffs} that \glspl{rc}
are more likely to contain design related changes. In Fig.~\ref{fig:oracle-dataset-per-project}, we observe
approximately the same ratio across each project. A notable outlier is the \jedit project which has almost the same
number of commits in each category. Rather than being a sign of superior oriented object design or a systematic
application of refactorings we believe this result is caused by the commit hygiene of the project. Indeed, this project
was rated with a \emph{Poor} commit hygiene, the developers often bundling many unrelated changes together in large
commits as demonstrated by a high percentage of tangled commits (\pct{70}). Thus, a commit is more likely to contain
design changes than a project with more commits. However, this means that recovering meaningful design information from
the diff in \jedit is also harder than in projects with a better commit hygiene because the changes are hidden by other
changes in the same commit.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/solid-count-per-project}
    \caption{The distribution of SOLID/NON-SOLID commits for each project.}
    \label{fig:oracle-dataset-per-project}
\end{figure}

% Fig.~\ref{fig:oracle-dataset-boxplot} informs us about the distribution of SOLID and NON SOLID commits across the
% different projects. We can observe that we obtain more or less the same number of SOLID and NONSOLID commits for each
% project after sampling. However, we note a tendency where the number NONSOLID commits instances is varies more than
% SOLID commits.

% \begin{figure}[ht]
%     \centering
%     \includegraphics[width=0.5\textwidth]{figures/solid-boxplot}
%     \caption{The distribution of SOLID/NON-SOLID commits across the projects}
%     \label{fig:oracle-dataset-boxplot}
% \end{figure}

Regarding the distribution of the type of SOLID principles in Tab.~\ref{tab:sample-size}, we can see that SRP instances
are more prevalent than any other type. By decreasing order, we found 290 instances of SRP, 106 instances of OCP, 46
instances of LSP, 23 instances of DIP, and 15 instances of ISP. Even though DIP and LSP applications are hard to find in
the source code, we haven't been confronted to many situations were this kind of design principle were applied.
Regarding DIP especially, we observed that dependency inversion was barely used even though it is considered to have
major benefits for the maintainability of the source code. Whether this apparent disparity comes from a conscious
decision from developers or pure happenstance is unclear. Design principles like SRP are notably easier to
understand and implement than DIP. However, the previous argument do not explain the low number of ISP instances found.
Applications of ISP are relatively easy to spot and easy to implement and understand for developers. We believe that
this design principle is too specific too be found in large quantities as a general principle such as SRP.

Concerning the commit hygiene scores, most of the projects we studied were decent (\emph{Good}, \emph{High}). Since the
hygiene score is determined subjectively by the reviewer, it is hard to definitely rank the projects but it is intended
as an estimate of what to expect in terms of comprehension difficulty if a researcher or developer was to read the
diffs. Moreover, this also means we cannot establish a formal correlation between the hygiene score and the percentage
of tangled commits for the projects studied. At first glance, it seems that there is no obvious correlation. We believe
it's due to the fact that not all tangled commits are equal. There is multiple ways for a commit to become tangled and
they are not equal it terms of added complexity when trying to understand the code changes they incur.

\section{Approach}
\label{sec:solid-approach}
To build our model, we use a decision tree approach from the family of supervised learning. We use decision trees for
their capacity to model complex phenomena and their explicability. Moreover, decision trees are great at filtering
relevant features which will inform us on which metrics are instrumental, among the 52 we extracted via \sourcemeter
that are using to train this model, in order to build a first intuition towards a theory of change patterns.

We train the model using the fluctuations for each commit as features and the oracle data collected previously as target
value. The target value is a binary value indicating that either a commit contains an application of SOLID principles or
it doesn't. We are not trying to detect which specific principle was applied at this time as we have a low class
representation for LSP, ISP, DIP. We use the metric fluctuations in \glspl{rc} because they have a high density of
design related changes compared to regular commits as found in Section~\ref{sec:explore-tradeoffs}. The advantage is
that we are more likely to have relevant examples for training and more balanced training sets. Before the model is
trained, we run hyperparameter optimization on the training set with cross-validation to select the best hyperparameters
for the decision tree. To mitigate the imbalance in SOLID and NON-SOLID commits, we use a weighted training and
evaluation method which mitigates the possible overfitting caused by unbalanced datasets~\cite{friedman2001elements}.

Since we are interested in a general model capable of predicting the application of design principles for commits of any
project, we have to evaluate its performance on new projects. Simply splitting the available data into training and
testing sets would not be an accurate evaluation of performance. Thus, we reserve the data for one arbitrary project for
testing (acting as the "new project") and use the remaining projects' data for the training. This process is repeated
such that each project is reserved in turn and tested as the "new project". The results for each experiment are then
averaged to give the final performance of our approach. This procedure guarantees that new commits are never seen during
hyperparameter optimization or training and make the most of our available dataset.

\section{Results}
We evaluate the performance of our models on several metrics. We use accuracy, the \gls{auc} of the \gls{roc}, often
abbreviated as \gls{auroc}~\cite{auroc}, and the F1 Score~\cite{f1score}. The results are shown in
Tab.~\ref{tab:model-performance}.

\begin{table}[ht]
    \centering
    \caption{Average performance of the approach with respective standard deviations.}
    \begin{tabular}{c|c|ccc}
    Accuracy & AUROC & F1 & F1 SOLID & F1 NON-SOLID \\ \hline\hline
    \pct{65.9} ($\pm$5) & \pct{68.8} ($\pm$7) & \pct{66.4} ($\pm$7) & \pct{58.8} ($\pm$8) &
    \pct{69.8} ($\pm$1)\\
    \end{tabular}
    \label{tab:model-performance}
\end{table}

We obtain an average weighted accuracy of \pct{65.9} which indicates that our model is somewhat capable of detecting the
application of SOLID principles. However, this does not inform us of the capacity of the model to distinguish between our
two class of data: SOLID and NON-SOLID. For this, we use \gls{auroc}. A value of \pct{50} signifies the model has no
class separation capacity, similar to a random classification. We use this value as our baseline. We obtained an average
of \pct{68.8} for \gls{auroc} for our model, confirming the capacity of the model of making adequate predictions most of
the time. Interpreted, it means our model has about \pct{68.8} of chance to predict the right class. The F1 score is
another measure of prediction performance based on the precision (number of selected items that are relevant) and recall
(number of relevant items that are selected). We obtain a value of \pct{66.4} which indicate again a certain capacity of
prediction for the model.

Finally, F1 SOLID and F1 NON-SOLID inform us on the capacity of the model to make predictions for each class. We see
that in average, our models had an easier time guessing \gls{rc} containing no application of SOLID principles as
suggested by the higher F1 score for NON-SOLID than SOLID. Additionally, we can see that the NON-SOLID predictions are
more reliably learned with a low standard deviation of \pct{1}. SOLID predictions, however, suffer from a high standard
deviation (\pct{8}) relatively to the average F1 SOLID score.

Next, we want to understand what metrics are most useful for the classification. To do this, we rank the metrics usage
by the classifiers in Tab.~\ref{tab:metrics}. For each model, we count the metrics in the first 3 levels starting from
the root that appear at least twice among all projects. An exact description of each metric is available on the official
documentation of \sourcemeter~\cite{sourceMeterDoc}.

\begin{table}[ht]
    \centering
    \caption{Number of occurrences for recurring metric in the three first levels of the decision trees.}
    \begin{tabular}{ll|c}
    Metric & Abbreviation & Occurrences \\ \hline\hline
    Number of Local Attributes & NLA & 8 \\
    Total Number of Methods & TNM & 7 \\
    Coupling Between Objects & CBO & 6 \\
    Total Number of Private Methods & TNPM & 6 \\
    Number of Local Public Attributes & NLPA & 5 \\
    Total Number of Local Attributes & TNLA & 4 \\
    Number of Incoming Invocations & NII & 4 \\
    Total Logical Lines of Code & TLLOC & 4 \\
    Total Number of Local Getters & TNLG & 3 \\
    Number of Parents & NOP & 3 \\
    Total Number of Attributes & TNA & 2 \\
    Total Number of Statements & TNOS & 2 \\
    Documentation Lines of Code & DLOC & 2 \\
    Number of Statements & NOS & 2 \\
    Public Documented API & PDA & 2 \\
    Number of Attributes & NA & 2 \\
    \end{tabular}
    \label{tab:metrics}
\end{table}

We observe that the most useful metrics are NLA and TNM. They are almost always present as one of the most decisive
variables. Moreover, we found that NLA was the root metric in 8 out of 11 times while TNM was in the one 3 remaining.
This is a surprising result for a size metric. We also see that most of the top metrics are concerned with either
attributes or methods. Only a handful are concerned with classes or documentation which is also counter intuitive since
we predicted the application of SOLID principles, inherently operating at a higher granularity than attributes and
methods. Overall, 11 of the metrics presented are measuring size. 2 metrics (CBO, NII) are measuring coupling. 2 metrics
(DLOC, PDA) are measuring documentation, and a single metric is measuring inheritance. There is no metric measuring
cohesion or complexity.
% Our current comprehension cannot explain why size metrics are given so much weight.
A possible explanation may be that other types of metrics are not precise enough given the cleanliness of the data
(tangled changes) so the model defaults to using size metrics as its best proxy. Another aspect to take into account is
the redundancy between some metrics (e.g., there are five metrics concerning attributes). It would be interesting to
force the model to use only one per type of object and see then which is the most relevant.

\section{Preliminary Evaluation of Usefulness}
Our objective is not only to provide a model that can generalize to other projects, but whose predictions are actually
helping the developers. In addition to the performance validation described in Section~\ref{sec:solid-approach}, we
investigate whether our model is able to give predictions that are relevant to developers for any commit (i.e., not only
\glspl{rc}) for new projects. In order to do this, we took a dogfooding approach where we apply our approach presented
in this chapter on our own projects~\cite{dogfooding}.

\subsection{Protocol}
T. Schweizer and V. Zafeiris will evaluate the predictions from the model on their respective projects \pa (356 commits
and 3977 LLOCs), and a proprietary project that measure software quality in governmental projects, anonymized \pb. In
parallel, we compute the metric fluctuations for the entire revision history of both projects using the procedure
described in Chapter~\ref{sec:datacol}. Then, we train a general model using the approach and the data presented in the
Sections~\ref{sec:oracle} and~\ref{sec:solid-approach} sections. Afterwards, we run the metric fluctuations of
\metrichistory and \pb through the classifier to obtain the predictions of the applications SOLID principles, and we
selected 10 commits classified as SOLID and 10 commits classified as NON-SOLID for each project. Finally, both
developers described the extent to which they agreed, or disagreed with the prediction of the classifier for the sample
of 20 commits from their respective projects:
\begin{itemize}
    \item if a commit was tagged as SOLID, whether they agreed that this commit contained changes related to design
    \item if the commit was tagged as NON-SOLID, whether they agreed that this commit did not contain changes related to
    design
\end{itemize}

We use a Likert scale~\cite{likert} with the options \emph{Disagree}, \emph{Somewhat disagree}, \emph{Neutral},
\emph{Somewhat agree}, and \emph{Agree} to qualify their agreement. \emph{Disagree} signifies a total disagreement while
\emph{Agree} signifies a total agreement with the question presented above.

\subsection{Results}
For each project, we count 1) the overall agreement, composed of \emph{Agree} and \emph{Somewhat agree}; 2) the overall
disagreement, composed of \emph{Disagree} and \emph{Somewhat disagree}; 3) the SOLID agreement, composed of \emph{Agree}
and \emph{Somewhat agree} responses on commits predicted as SOLID; 4) the NON-SOLID agreement, composed of \emph{Agree}
and \emph{Somewhat agree} responses on commits predicted as NON-SOLID. The results are presented in
Tab.~\ref{tab:agreement}. We also show the decision tree model in Fig.~\ref{fig:tree-model-userstudy}.

\begin{table}[th!]
    \centering
    \caption{Tally of the developers opinions for projects \pa et \pb.}
    \begin{tabular}{l|ll}
     & \pa & \pb \\ \hline \hline
    Overall agreement & \valpct{13}{65} & \valpct{16}{84} \\
    Overall disagreement & \valpct{7}{35} & \valpct{3}{16} \\ \hline
    SOLID agreement & \valpct{4}{40} & \valpct{7}{70} \\
    NON-SOLID agreement & \valpct{9}{90} & \valpct{9}{90}
    \end{tabular}
    \label{tab:agreement}
\end{table}

\begin{figure}[bh!]
    \centering
    \includegraphics[width=1\textwidth]{figures/tree-study}
    \caption{The decision tree model used for the evaluation.}
    \label{fig:tree-model-userstudy}
\end{figure}

Globally, \pb contains the only instance where the developers had no opinion about a prediction. Looking at the overall
agreement and disagreement, we can see that developers were mostly agreeing with the results of the classifier as
demonstrated by a combined total of $29$ ($13 + 16$) positive opinions versus $10$ ($7 + 3$) negative opinions. This is
an encouraging finding that signifies that the classifier was relevant in its task and was able to give useful results
in practice.

Looking at the SOLID agreement, we observe that the model had equivocal results. In \pa slightly less than half
of the commits classified as containing SOLID principles actually contained relevant design knowledge for the developer,
while in \pb, more than half of the commits classified as containing SOLID principle were actually useful for the
developer. Given our limited sample of projects, this can indicate that the classifier performs differently depending on
the project but also that we need to gather more opinions for a given commit from multiple developers as their opinion
is, definition, subjective. However, the NON-SOLID agreement seems to be converging for both developers. Indeed they
agreed with the prediction of the classifier for almost all commits classified as NON-SOLID. This result aligns with the
F1 scores obtained previously which indicated that our model performed better in detecting NON-SOLID instances than
SOLID instances.

We provide the model as well as the script to generate it in a replication package on Zenodo~\cite{solidModelDataset}.

\section{Threats to validity}
In Section \ref{sec:threats-validity}, we described the threats to validity related to the mining of the
fluctuations in metrics. In this section, we will address the threats to validity affecting the creation of the
annotated dataset, and the user study.

The process of detecting design intent and applications of SOLID principles is subject to researcher bias which
constitutes a threat to internal validity. To mitigate this, a calibration analysis was exercised independently by two
reviewers on all the \glspl{rc} of \jfreechart. Then we proceeded to resolve any conflicts by consensus and and created a
common annotation protocol as a result. The full protocol is available in Appendix~\ref{sec:appendix-solid}.

The usefulness study is also threatened by researcher bias since the developers are also the researchers of the study.
However, the objective of this usefulness study was not to prove the general relevance and usefulness of our model but
to verify that the predictions of the classifier were sound and could be transposed to real projects.

\section{Lessons Learned}
In this chapter, we put the observations from the exploratory studies in Chapter~\ref{sec:preliminary} into practice. We
were able to demonstrate that metric fluctuations can be used as indicators of design knowledge, SOLID design principles
in our case (RQ4). The results we obtained are encouraging despite their limitations, and given that we made certain
simplifications to our approach such as using a naïve aggregation process and considering commits containing multiple
sets of unrelated changes. In Chapter~\ref{sec:conclusion}, we propose certain possible avenues for improvement that
could result in better prediction performance, especially for detecting positive instances of SOLID applications.
% However, even though the model does not perform as well as one might hope, it is still useful to accomplish the

During the process of creating the oracle, we noticed that it was sometimes possible to guess when a functionality was
moved around using only the changes in lines of code. Most of the time, it's only a few lines that change across files:
the developer will wrap an if condition around a group of statements or some functionality will be tweaked; radical
changes are rather an exception. Thus, the displacement of a concept to other files in the project will stand out from
the usual editing pattern described above. We think it's a similar process that made our model privilege size metric
changes than the coupling, cohesion, and inheritance metrics we expected. We believe that when we will refine our
methodology, we will be able to tap into the metrics that are eluding the model currently. Moreover, this is also a sign
that reinforces the intuition presented in Chapter~\ref{sec:introduction} that fluctuations in metrics can be used to
trace macroscopic changes relevant to developers.
