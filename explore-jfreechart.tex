\section{Exploring \jfreechart}
\label{sec:explore-jfreechart}
In this section, we present an exploratory study to estimate \glspl{rc}' contributions to design quality through the use
of internal quality metrics. We studied the development history and refactoring activity of JFreeChart~
\footnote{\url{http:/www.jfree.org/jfreechart/}}, a well-studied~\cite{AlDallal18,xing06} software project in order to
answer RQ1.

\subsection{Setup}
\subsubsection{Project selection}
We selected the open-source project \jfreechart. It provides a chart library written in Java which enables developers to
integrate professional-looking charts in their programs. This project has been studied extensively by the refactoring
community~\cite{AlDallal18,xing06}. Its medium size ($\sim$600 classes) and history (over 10 years old) is ideal. It is
big enough to be relevant in quantitative analysis, while being small enough to allow manual and qualitative analysis.
Its size may also support relatively strong conclusions and help to guide our future studies. The project has been used
by a variety of applications from different domains over the years and is still actively
developed\footnote{\url{http://www.jfree.org/jfreechart/users.html}}.

\subsubsection{Objects}
The \jfreechart project is composed of two source code repositories, two bug trackers, mailing lists, a forum, and a
website:

\noindent\textit{Repositories}
\begin{itemize}
  \item \url{https://sourceforge.net/p/jfreechart}
  \item \url{https://github.com/jfree/jfreechart}
\end{itemize}

\noindent\textit{Bug trackers}
\begin{itemize}
  \item \url{https://sourceforge.net/p/jfreechart/bugs}
  \item \url{https://github.com/jfree/jfreechart/issues}
\end{itemize}

\begin{description}
    \item[Mailing lists]: \url{https://sourceforge.net/p/jfreechart/mailman}
    \item[Forum]: \url{http://www.jfree.org/forum/index.phps}
    \item[Website]: \url{http://www.jfree.org/jfreechart/}
\end{description}

This project has a particularity: The development started on SourceForge and was then imported to GitHub. However, the
content of the bug tracker was not imported to GitHub at once; they gradually stopped using the one provided by
SourceForge and moved gradually to the one provided by GitHub. As a result, the source code and the issue repositories
are split between the two platforms.

Thus, we selected all the revisions available on the GitHub repository before 2018-05-01. This selection contains
$3\,646$ revisions covering over 10 years of development in a mature project. Each revision is characterized by its
source code, comments, commit message, and updates to the changelog (this artifact is edited by the developers to detail
the modifications to the source code for every revision; it is stored in the same repository as the source code).

\subsection{Computing the dataset}
\label{sec:studydesign_scenarios}
\label{sec:studydesign_metrics}
Using the procedure described in Chapter~\ref{sec:datacol}, we calculated the metric fluctuations at the commit level
for all commits in the GitHub repository before 2018-05-01 for the metrics \gls{cbo}, \gls{dit}, \gls{lcom5}, and
\gls{wmc}. These quadruples are further used to proxy the direction of change in internal design quality. The focus is
on the direction of change that constitutes a trend, rather than on change magnitude. To better understand such trends,
we defined four intuitive scenarios to classify the patterns of activity for a \gls{rc}, given the metric
changes. These scenarios are described in the next paragraphs and summarized in Tab.~\ref{tbl:scenario-summary}.

\begin{table*}
    \centering
    \caption{Summary of the scenarios}
    \resizebox{\textwidth}{!}{\begin{tabular}{c l}
    \textbf{Scenario} & \textbf{Definition} \\ \hline \hline
      1 & No metric changes \\
      2 & One metric changes \\
      3 & At least two metrics change, and the changes are in the same direction (all improve or all worsen) \\
      4 & At least two metrics change, and the changes are in mixed directions (some improve and some worsen) \\
    \end{tabular}}
    \label{tbl:scenario-summary}
\end{table*}

\paragraph{Scenario 1: \glspl{rc} with no change in metrics}
An example of this scenario is a revision where a refactoring was found to have been applied, but no change in any of
the selected metrics was found. This is the case for refactorings like renames. Based on the metrics we have selected,
Scenario 1 instances are not normally expected to represent important design decisions, but rather pure functionality
addition or understandability enhancements.

\paragraph{Scenario 2: \glspl{rc} with a change in a single metric}
In this scenario, we include \glspl{rc} that affect a single metric, positively or negatively. Especially, in the case
of positive impact, these instances could correspond to targeted changes to specifically improve the particular metric.
While this shows clear intent, the intent is not necessarily related to design decisions.

\paragraph{Scenario 3: \glspl{rc} where all metrics change monotonically towards improving or declining direction.}
This scenario includes \glspl{rc} where more than one metric was impacted. A special inclusion condition is that all the
affected metrics should have changed towards the same direction, either all positively or all negatively. Similar to
Scenario 2, \glspl{rc} in this scenario show clear intent. However, due to the scale of change and the impact on metrics, the
intent is more inclined to be closer to a design decision.

\paragraph{Scenario 4: \glspl{rc} where multiple metrics change in different directions.}
Scenario 4 is the same as Scenario 3 in terms of multiple metrics being affected, with the important difference that not
all metrics change towards the same direction. One popular example is the metrics for cohesion and coupling, which in
many cases change at the same time, but in opposite directions, especially during remodularization
tasks~\cite{stroggylos07}. In our view, these instances are the most interesting ones, as they indicate conflicting
goals.

In the context of our work, we call instances of Scenario 4, "\emph{design tradeoffs}". Indeed, we established earlier
that changes in internal quality metrics in opposite directions translate to tradeoffs in internal quality, which are
likely to capture design tradeoffs.. In practice, a design tradeoff is a situation where a change, i.e., a refactoring
action, would result in a controversial impact to design quality; while some dimensions are improved, others may
deteriorate. In this situation, the developer will have to make a decision as to which metrics and quality aspects are
more important than others (given the current requirements) and eventually settle for specific tradeoffs. This is why we
consider instances in Scenario 4 to be closely related to design decisions. It is also possible that design decisions
also appear in instances of Scenario 3, where there is no tradeoffs but a clear direction of changes measured.

% To automatically classify instances in scenarios, we
% associate each revision with a tuple:
% % devised the following simple method: \\
% % Let each revision be defined as a tuple
% \[ s = (WMC, LCOM, CBO, DIT, hit\_count)  \]
% where \texttt{WMC}, \texttt{LCOM}, \texttt{CBO}, \texttt{DIT} are the
% respective sums of the metric's difference for each class that changed in the
% revision. \texttt{hit\_count} counts the number of metrics affected by changes
% for a revision. It is bound in \texttt{[0,4]}.
% Given this definition, then:
% \begin{itemize}
% \item Scenario 1 contains the revisions with $s_1 = (0, 0, 0, 0, 0)$.
% \item Scenario 2 contains the revisions with $s_2 = (\_, \_, \_, \_, 1)$ where
% $\_$ can by any value.
% \item Scenario 3 contains the union of revisions where changed metrics have
% changed in the same direction, either increasing, i.e.,
% $s_{3i}=(\leq 0, \leq 0, \leq 0, \leq 0, >1)$
% or decreasing, i.e.,
% $s_{3d}=(\geq 0, \geq 0, \geq 0, \geq 0, >1)$.
% \item Scenario 4 contains the revisions, where in $s_4$ at least one metric has changed
% positively ($>0$), and at least another metric has changed negatively ($<0$),
% thus, in $hit\_count >1$.
% \end{itemize}

\subsection{Manual Evaluation}
\label{sec:explore-manual-eval}
We manually analyzed each \gls{rc} to identify the design intent behind applied refactorings. We based our
analysis on code and comment inspection, commit messages, and the changelog of refactored classes. Specifically, we
studied the developers' design intent from two perspectives:
\begin{enumerate}
  \item The involvement of design decisions in the refactoring process, i.e.,
  whether the developer applied the identified refactorings as part of
  introducing new design decisions or enforcing design decisions that were
  established in previous revisions.

  \item The type of implementation task the developer was engaged in, while
  changing code structure through refactoring, i.e., whether any design
  decisions were enforced as part of (a) refactoring low quality code, (b)
  implementing new features, or (c) fixing bugs.
\end{enumerate}

The detection of design decisions in \glspl{rc} is a rather challenging task since it requires understanding not only
the changed code parts, but the overall design of affected classes. Moreover, determining whether a set of refactorings
enforce a past design decision, requires tracing back to previous revisions of refactored code. A successful strategy to
improve this process was to begin the analysis with the oldest refactoring and then go forward in time: This helps the
reviewer to understand the evolution of the design. Additionally, the developers of \jfreechart scrupulously maintain a
changelog of their source code changes at the project and file level, giving us insights about their intents.

In order to reduce the subjectivity of this process, the evaluation was performed independently by two of the authors
and it was followed by a strict conflict resolution procedure. The inter-rater agreement between their assessments was
initially moderate, indicated by a value of 0.49 for Cohen’s Kappa~\cite{cohen1960coefficient, landis1977measurement}.

\subsection{Results}
We have automatically analyzed 3\,646 commits in the version history of \jfreechart with an extended version of
\rminer~\cite{rminer18}. The tool identified 247 refactoring operations in the production code that were distributed
across 68 revisions. The automatically identified refactorings were manually validated and eight of them (7 cases of
\textsc{Extract Method}, 1 case of \textsc{Rename Method}) were rejected as false positives. The \emph{refactoring
revisions} containing them did not include any true positives and were also rejected from further analysis (4
revisions). Tab.~\ref{tbl:refactorings} presents the distribution of true positives to different refactoring types in
the 64 remaining \glspl{rc}. The 64 \glspl{rc} were further processed in order to measure the
differences of internal metrics for all changed classes, as explained in Section~\ref{sec:studydesign_metrics}.

\begin{table}[t]
\centering
\caption{Refactoring operations in \jfreechart{}} \label{tbl:refactorings}
\begin{tabular}{ l r }
 Refactoring Type & Count \\
 \hline \hline
Extract And Move Method	& \valpct{6}{2.5} \\
Extract Method	 & \valpct{80}{33.5} \\
Extract Superclass	& \valpct{2}{ 0.8} \\
Inline Method	& \valpct{4}{ 1.7} \\
Move Class	& \valpct{20}{ 8.4} \\
Move Method	& \valpct{6}{ 2.5} \\
Move Source Folder	& \valpct{1}{ 0.4} \\
Pull Up Attribute	& \valpct{12}{ 5.0} \\
Pull Up Method	& \valpct{14}{ 5.9} \\
Rename Class	& \valpct{15}{ 6.3} \\
Rename Method	& \valpct{79}{33.0} \\ \hline
\textbf{Total}	& \valpct{239}{100} \\
\end{tabular}
\end{table}

We then automatically classified each \gls{rc} to one of the four scenarios introduced in
Section~\ref{sec:studydesign_scenarios}. We show the classification in Tab.~\ref{tbl:scenarios}. Noticeably, a large
part of \glspl{rc} (\pct{29.7}) do not involve changes to internal metrics (Scenario 1). Source code changes
in these revisions are due to rename and move class refactoring operations. \glspl{rc} with a single changed
metric (Scenario 2), amount for \pct{35.9} of total revisions. These revisions involve mainly extract method
refactorings that affect the WMC metric. Revisions classified to Scenario 3 make up \pct{25} of the total. In them,
developers applied a more extensive set of refactoring operations, such as \textsc{Move Attribute/Method},
\textsc{Extract Superclass}, and \textsc{Move Class}. Such refactorings have a combined effect on internal metrics,
either improving or deteriorating all of them. Finally, we found that in \pct{9.4} of \glspl{rc} multiple
metrics are changed towards different directions. Such revisions usually involve \emph{design tradeoffs}, i.e.,
improvement of a design property of one or more classes at the expense of deteriorating another. For instance, a
\textsc{Move Method} refactoring may improve the cohesion of the origin class at the expense of increasing the coupling
of the destination class.

\begin{table}[t]
  \centering
  \caption{\glspl{rc} for each Scenario} \label{tbl:scenarios}
  \begin{tabular}{ l r }
   Scenario & Revisions (\%) \\
   \hline \hline
  1 &  \valpct{19}{29.7} \\
  2 & \valpct{23}{35.9} \\
  3 & \valpct{16}{25.0} \\
  4 & \valpct{6}{ 9.4} \\
  \end{tabular}
  \end{table}

We summarize the types of implementation tasks that developers were involved in \glspl{rc} in
Tab.~\ref{tbl:implementation_tasks}. We determined the type of implementation task through inspection of code
differences combined with analysis of commit logs, and embedded change logs of refactored classes. In several cases,
commit and change logs included references to issue tracking identifiers. Revisions with a pure refactoring purpose
(termed ``root canal'' by~\cite{murphyhill12}) correspond to \pct{46.9} of total revisions. Most of these revisions (20
out of 30) involved only renaming operations, while the rest applied \textsc{Extract/Inline/Move Method} refactorings.
Simple refactorings (\textsc{Extract/Move Method}) are also applied within revisions that focus on fixing bugs. The most
complex and, also, interesting cases of refactorings are part of revisions that focus on new feature implementation
tasks (termed ``flossing'' by~\cite{murphyhill12}). These revisions correspond to \pct{45.3} of the total and involve
moving state and behavior among classes, as well as, superclass extraction in class hierarchies. We discuss the most
interesting of these cases that are also characterized by design tradeoffs in Section~\ref{sec:results_cases}.

\begin{table}[t]
\centering
\caption{Implementation tasks and \glspl{rc}}
\label{tbl:implementation_tasks}
\centering
\begin{tabular}{ l r }
 Task type & Revisions (\%) \\
 \hline \hline
Refactoring &  \valpct{30}{46.9} \\
Feature Implementation & \valpct{29}{45.3} \\
Bug Fix & \valpct{5}{ 7.8} \\
\end{tabular}
\end{table}

\subsubsection{Interesting cases}
\label{sec:results_cases}
Our manual evaluation of revisions revealed several design decisions related to the refactorings that we detected. In
this section, we select and explain interesting design decisions identified in \glspl{rc} from Scenarios 3-4.
Moreover, we discuss the effect on internal metrics of the refactorings applied in each revision. We summarize these
revisions in Tab.~\ref{tbl:revisions_tradeoff}. Each revision is given a number, which we use in the rest of the text
for identification. Further, for each one, in Columns 2--7, we list under what scenario it was classified, its
abbreviated Git Commit ID and the aggregate metric differences. For each revision, we display its key take-away in a
boxed sentence.

The first two revisions were classified in Scenario 3 and include some interesting design decisions. The remaining four
revisions were classified in Scenario 4. One of these revisions, R3, involves one of the most complex refactorings in
the revision history of \jfreechart.

\begin{table}[t]
\centering
\caption{Metric fluctuations in interesting cases}
\label{tbl:revisions_tradeoff}
\centering
\begin{tabular}{l l l r r r r}
 Id & Scenario & Commit & WMC & LCOM5 & CBO & DIT \\
 \hline \hline
R1 & 3 & 4c2a050 & 10 & 3 &  25 &  18  \\
R2 & 3 & 74a5c5d &  4 &  2 &  2 &  0  \\
R3 & 4 & 1707a94 & -9 & -1 &  5 &  2  \\
R4 & 4 & 202f00e &  1 &  0 & -1 &  0  \\
R5 & 4 & 528da74 & -2 & -2 &  1 & -1 \\
R6 & 4 & efd8856 & 12 & -3 &  0 &  0 \\

\end{tabular}
\end{table}

\paragraph{\textbf{Revision R1}}

In this revision, an \textsc{Extract Superclass} refactoring unifies under a common parent, the \TextAnnotation{} and
\AbstractXYAnnotation{} class hierarchies, as well as the individual class \CategoryLineAnnotation{}. This way, a larger
class hierarchy is formed having the extracted superclass \AbstractAnnotation{} as root. The refactoring was motivated
by the need to add an event notification mechanism to plot annotation
classes\footnote{\url{https://sourceforge.net/p/jfreechart/patches/253/}}. The developers decided to add this feature to
all plot annotation classes through its implementation in a common superclass (\AbstractAnnotation{}). The
implementation comprises appropriate state variables and methods for adding/removing listeners and firing change events.
The new feature increased the DIT value of all \AbstractAnnotation{} subclasses, as well as their coupling (CBO) due to
invocations of inherited methods. The negative impact on WMC and LCOM5 metrics is due to extra functionality added to
client classes of the new feature (e.g. \Plot{}, \CategoryPlot{}).

\vspace{0.05in}
\begin{mdframed}
Revision R1 shows an occurrence of a design decision
that spans over multiple classes where there is no tradeoff with respect to
metrics.
\end{mdframed}

\begin{figure*}
 \setlength{\abovecaptionskip}{-5pt}
 \setlength{\belowcaptionskip}{0pt}
  \begin{center}
    \includegraphics[width=0.8\textwidth]{figures/figure1}
  \end{center}
  \caption{Revision R1.}
  \label{fig:visitor}
\end{figure*}

\paragraph{\textbf{Revision R2}}
This revision involves two \textsc{Pull Up Method} refactorings from \AbstractCategoryItemRenderer{} to the parent class
\AbstractRenderer{}. The refactorings enable reuse of functionality related to adding rendering hints to a graphics
object. The functionality was introduced in a previous revision to \AbstractCategoryItemRenderer{} and is reused in
order to provide hinting support to all renderers. In revision R2 the methods are invoked from \AbstractXYItemRenderer{}
and its subclass \XYBarRenderer{}. The refactorings added extra methods to \AbstractRenderer{} and, thus, increased the
values of WMC, LCOM5 and CBO metrics. Although metric values were improved (negative change) for
\AbstractCategoryItemRenderer{}, the aggregate change values for the revision are still positive due to method
declarations and invocations in \AbstractXYItemRenderer{} and \XYBarRenderer{}.

\vspace{0.05in}
\begin{mdframed}
Revision R2, while very similar to R1, shows that the direction of changes happening at the class granularity can be
masked by the revision granularity in the same design decision.
\end{mdframed}

\paragraph{\textbf{Revision R3}}
This revision includes 20 refactoring operations comprising 1 \textsc{Extract Superclass}, 1 \textsc{Extract Method}, 1
\textsc{Rename Method}, 10 \textsc{Pull Up Attribute} and 8 \textsc{Pull Up Method}. The refactoring inserts an
intermediate subclass (\DefaultValueAxisEditor{}) between \DefaultAxisEditor{}, the hierarchy root, and
\DefaultNumberAxisEditor{}, its direct child. The new parent of \DefaultNumberAxisEditor{} absorbs a large part of its
state and behavior. The refactoring was motivated by the need to introduce a properties editing panel for the
logarithmic scale numeric axis. The new panel (\DefaultLogAxisEditor{}) has overlapping functionality with
\DefaultNumberAxisEditor{}. This functionality is reused through inheritance and \DefaultLogAxisEditor{} is implemented
as a subclass of \DefaultValueAxisEditor{}. Moreover, the developers decided to reuse \DefaultNumberAxisEditor{}
functionality through a new parent class, in order to maintain the abstraction level of the hierarchy root. However, the
CBO and WMC of \DefaultAxisEditor{} have increased, since it, also, serves as a factory for creating instances of its
subclasses. The positive impact on metrics in revision R3 (reduction of WMC, LCOM5) is dominated by the simplification
of the \DefaultNumberAxisEditor{} implementation due to pull up refactorings.

\vspace{0.05in}
\begin{mdframed}
Revision R3 shows a design decision spanning over multiple classes where there is a trade-off in metrics. Moreover, it
shows that examining metrics at revision level can mask important details happening in smaller levels. Additionally,
this is an example of tangled commit where an implementation is also added for PolarPlot editor.
\end{mdframed}


\paragraph{\textbf{Revision R4}}
The focus of code changes in this revision is the simplification of the API that \Plot{} class provides to its
subclasses. The applied refactorings extract the notify listeners functionality to a new method, \fireChangeEvent{} with
protected visibility. Although the implementation of the extracted method is rather simple, it replaces the notification
logic in fifteen locations in the \Plot{} class and in several locations in its subclasses \CategoryPlot{},
\FastScatterPlot{} and \XYPlot{}. Moreover, it decouples \Plot{} subclasses from the implementation of the change event.
The refactoring increases the WMC of \Plot{} due to the new method declaration and decreases the CBO of its subclasses
due to the removal of references to the change event implementation (\PlotChangeEvent{}). We note that due to unused
imports of the \PlotChangeEvent{} class in \Plot{} subclasses, the \sourcemeter tool does not recognize the reduction of
CBO in all cases.

\vspace{0.05in}
\begin{mdframed}
Revision R4 shows a design decision affecting multiple classes where the trade-off is between two metrics only.
Additionally, this is a revision where there is no granularity conflict between revision and classes. This represents a
best case: the revision contains only the refactoring implementation which corresponds to a single design decision
that is represented by a metric trade-off.
\end{mdframed}

\paragraph{\textbf{Revision R5}}
In this revision, the identified refactorings involve moving an attribute and two methods, relevant to rendering a zoom
rectangle, from \ChartViewerSkin{} to \ChartViewer{} class. The \ChartViewerSkin{} is removed from project and
\ChartViewer{} is turned from a UI control to a container for the layout of chart canvas and zoom rectangle components.
The simplification of \ChartViewer{} is responsible for the improvement of WMC, LCOM5 and CBO in revision R5. However,
the CBO improvement has been counterbalanced due to another refactoring, not detected by \rminer, that implements a
second design decision within the same revision. The refactoring involves the move and inline of two \ChartCanvas{}
methods in the \DispatchHandlerFX{} class. The methods are related to dispatching of mouse events and their relocation
introduces a \emph{Feature Envy} code smell in \DispatchHandlerFX{} and respective increase in the CBO metric.
Nevertheless, this solution is preferred since it enforces a basic decision in the design of ChartCanvas: its behavior
related to user interaction should be dynamically extensible through registration of \AbstractMouseHandlerFX{}
instances.

\vspace{0.05in}
\begin{mdframed}
Revision R5 shows two design decisions affecting multiple classes resulting in a classification into Scenario 4. If only
one design decision where to have been implemented, it would have been categorized as Scenario 3.
\end{mdframed}

% tangled commit, two features added according to change log
% Revision comment: Change ChartViewer from Control to Region
% ChartViewer log: Change base class from Control to Region. ChartViewerSkin.java
% *               is deleted, code from that class is now included here (DG);
% ChartCanvas log: Add methods for auxiliary handlers, move dispatch handling
% *               methods to DispatchHandlerFX (DG);
% Reverse comment is in DispatchHandlerFX

\paragraph{\textbf{Revision R6}}
Finally, this revision includes a \textsc{Move Method} refactoring from \SWTGraphicsTwoD{} to \SWTUtils{}. The
refactoring enforces the decision that reusable functionality related to conversions between AWT and SWT frameworks
should be located in \SWTUtils{} class. The move method lowers the complexity and improves the cohesion of
\SWTGraphicsTwoD{}, although its WMC value is not changed due to extra functionality added in the same revision. On the
other hand, the cohesion of \SWTUtils{} is slightly changed contributing, thus, to the tradeoff between WMC and LCOM5 at
revision level.

\vspace{0.05in}
\begin{mdframed}
Revision R6 shows a design decision paired with a feature implementation creating an opposite change for one metric at
the class granularity.
\end{mdframed}
\vspace{0.05in}

In-depth inspection of revisions R1--R6 lead us to noteworthy observations on the presence of design decisions and the
hints that refactorings and metric fluctuations provide for their identification. First of all, combined fluctuations of
DIT and CBO within revisions, as is the case in \{R1, R3\} provide evidence of structural changes potentially related to
design decisions. The type and target of refactoring operations can contribute to tracing the classes affected by these
decisions. On the other hand, fluctuations of WMC and LCOM5 metrics, usually indicating changes to class
responsibilities, provide a strong indication of design decisions when they cause tradeoffs with other metrics (e.g,
R3--R5). However, the impact of refactorings to fluctuations of WMC and LCOM5 is often obscured by code additions that
implement new features. The problem is exaggerated in \glspl{rc} with tangled changes, as is the case in \{R3, R5\}.

We also observe that interesting decisions are identified in \glspl{rc} that implement new features.
Moreover, given that refactorings R4--R6 enforce past decisions, the identification of recurrent patterns of past
revisions provide stronger hints on the importance of decisions.

In conclusion, the qualitative analysis gives us confidence that the phenomenon is real and worth further inspection.
