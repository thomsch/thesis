target=thesis

all:
	pdflatex $(target).tex
	bibtex $(target).aux
	pdflatex $(target).tex
	pdflatex $(target).tex

new: cleanall all

clean:
	rm -rf $(target).aux  $(target).snm $(target).nav $(target).toc $(target).out $(target).bbl  $(target).blg  $(target).log $(target).nav $(target).snm $(target).toc *~ *.backup

show:
	evince $(target).pdf

cleanall: clean
	touch $(target).pdf
	rm  $(target).pdf

x: cleanall all show

e: cleanall all show
	make cleanall
