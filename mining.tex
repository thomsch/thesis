\chapter{Data collection}
\label{sec:datacol}
In this chapter, we present our overall study design. We describe our processes and the different datasets created. We
specify the scope, the population and its sampling, and the data collection techniques. Our approach consists of six
steps: A) Commit selection, B) Refactoring identification, C) Mining metrics D) Converting metrics' format, E) Computing
changes, and F) Aggregating metrics. We detail them in the following sections. Fig.~\ref{fig:dataset} provides
an overview of our approach and datasets.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/datasets.pdf}
    \caption{Different datasets mined from the source code of the project}
    \label{fig:dataset}
\end{figure}

\section{Design}
\subsection{Unit of analysis}
Our unit of analysis is the \emph{commit} or \emph{revision}. In version control systems, a revision represents the list
of differences for the files that changed in the project from a previous -- also known as parent -- version to the next
version. Revisions are groups of incremental changes between a pair of versions, each revision represents an atomic unit
of work, usually containing a cohesive set of changes although it is not always the case in practice~\cite{arima2018}.
We can think of the list of revisions of a project as its evolutionary history; each revision embodies a version of the
software.

\subsection{Target population}
\label{sec:exploration-project-selection}
We aim to understand the patterns of metric fluctuations in software projects written in Java under version control. The
target population of our study is therefore the set of all revisions present in the version histories of such projects.
The version history contains the incremental changes applied to the project until its latest state of development as
explained in the previous paragraph.

\subsection{Sampling technique}
We selected revisions from the version history of a sample of 13 popular open-source Java projects. The selection
was conducted using a mix of \emph{convenience}, \emph{maximum variation}, and \emph{critical} sampling based on a blend
of several attributes such as projects' popularity amongst developers, usage as research subjects, size, number of
contributors, platform, development style, and type (e.g., library, desktop application). Each project has a website and
a public repository of source code under an arbitrary version control system -- The type of the VCS was not a criterion.

\section{Project selection}
We selected 13 popular open-source Java projects.
% The selection was conducted using a mix of \emph{convenience},
% \emph{maximum variation}, and \emph{critical} sampling based on a blend of several attributes such as projects'
% popularity amongst developers, usage as research subjects, size, number of contributors, platform, development style,
% and type (e.g., library, desktop application). Each project has a website and a public repository of source code under
% an arbitrary version control system. The type of the VCS was not a criterion.
 The projects are listed in Tab.~\ref{tbl:project-list} with the
branch, source code location, number of commits mined and size. The projects also came with optional issue trackers,
mailing lists, forums, or changelogs (i.e., files maintained by developers to keep track of changes happening in
commits). For projects that use Subversion (\jedit, \argouml) as their \gls{vcs}, we first migrated their version
history to Git using GitHub Importer~\cite{githubimporter}.

\begin{table}
    \centering
    \caption{List of the software projects retained.}
    \tiny
    \begin{tabular}{l|lrrr}
        Project & Source code & Branch & Commits & Size (SLOC) \\
        \hline \hline
        \ant & \url{https://github.com/apache/ant} & master & 14\,234 & 139k \\
        \xerces & \url{https://github.com/apache/xerces2-j} & trunk & 5\,508 & 142k \\
        \argouml & \url{http://argouml.tigris.org/source/browse/argouml/trunk/src/} & trunk & 17\,797 & 176k \\
        \daggertwo & \url{https://github.com/google/dagger} & master & 1\,969 & 74k \\
        \hibernate & \url{https://github.com/hibernate/hibernate-orm} & master & 9\,320 & 724k \\
        \jedit & \url{https://sourceforge.net/p/jedit/svn/HEAD/tree/jEdit/trunk/} & trunk & 22\,873 & 124k \\
        \jena & \url{https://github.com/apache/jena} & master & 7\,112 & 515k \\
        \jfreechart & \url{https://github.com/jfree/jfreechart} & master & 3\,640 & 132k \\
        \jmeter & \url{https://github.com/apache/jmeter} & trunk & 15\,898 & 133k \\
        \junit & \url{https://github.com/junit-team/junit4} & master & 1\,972 & 30k \\
        \okhttp & \url{https://github.com/square/okhttp} & master & 1\,951 & 61k \\
        \retrofit & \url{https://github.com/square/retrofit} & master & 1\,038 & 20k \\
        \rxjava & \url{https://github.com/ReactiveX/RxJava} & 2.x & 4\,137 & 276k \\
    \end{tabular}
    \label{tbl:project-list}
\end{table}

\section{Commit selection (A)}
\label{sec:versions}
After downloading the projects onto disk, we extracted the commits from the default branch of each of the 13 projects
until 2018-12-31 (included). A branch represents an independent line of development~\cite{gitbook2014}. We focus on the
default branch because it is often this rendition of the development that will be released to the public. It represents
the mature code changes of a project. From this set, we exclude commits that are the results of a \emph{merge
operation}. In VCS, ``merging'' means to integrate all the changes from a ``source'' branch to a ``destination'' branch
in a single commit. The resulting commit is generally hard to read for humans and presents little interest for analysis
as the individual changes can be found on the source branch.
% Moreover, by downloading a version-controlled project -- also known as a
% \emph{clone} or \emph{checkout} -- we automatically download its entire history of changes.

We used the following command to obtain the list of commits to be analyzed for each project (where \texttt{<branch>} is
the name of the default branch):
\begin{lstlisting}[basicstyle=\ttfamily\small, keywordstyle=none]
    git log <branch> --pretty="%H" --no-merges --until="2018-12-31"
\end{lstlisting}

Overall, the 13 projects represent a cumulative $107\,449$ versions of projects spanning 20 years of software development
history.

% \todo{VSC is a non linear form of storing versions of a system. (Used to explain our choice for the mining)}
% For each project, we used the entire commit history that was publicly available from the project's start until April
% 2018.  We give more information
% about each project, including which branches we used, in Table \ref{tbl:project-list}.
% %
% Due to the large size of the data collected by our study, we intend to make it available upon publication via
% BitTorrent\footnote{\url{http://academictorrents.com/}}.

\section{Refactorings (B)}
As explained in the introduction, our study is interested in the refactoring activity of developers. To isolate the
revisions containing refactorings, we used \rminer\cite{rminer18}, a specialized tool that can detect refactorings
automatically in the history of a project with high recall and precision compared to other similar tools, such as
RefDiff~\cite{Silva17}. We ignore refactorings related to tests because we are not studying the role of tests in a
software's design. The refactoring detection yields a list of revisions containing at least one refactoring. Henceforth,
we refer to these revisions as \gls{rc}. Each of these \gls{rc} is also accompanied by a detailed list of the
refactorings it contains.

% Using \rminer, we thus filtered the 80\,508 commits across all the projects, down to 5\,921 \glspl{rc}, i.e., revisions
% containing at least one refactoring. The median percentage of \glspl{rc} compared to the entire revision
% history is \pct{7.35}.  The project with the lowest percentage is \jfreechart with \pct{2.19} and highest is \daggertwo
% with \pct{17.36}. This gap can have many causes including the accuracy of \rminer, the complexity of refactorings used
% by the developers, and their development habits and guidelines.

\section{Native dataset (C)}
We used \textit{\sourcemeter}~\cite{sourceMeter} to calculate the software metrics for individual revisions. We
automated the execution of \sourcemeter to run in batch multiple commits of a project with our command line tool
\metrichistory~\cite{metricHistory}.

Calculating the metrics for thousands of revisions is a computationally intensive task, especially for large projects
(e.g. \jedit, \hibernate). It can be seen as the equivalent of calculating the metrics for $107\,449$ projects. To
accommodate this process, we built a distributed computation system based on the Akka toolkit and runtime~\cite{akka}.
Each computation node invokes \metrichistory tasks to calculate the metrics for individual revisions of a project.
Job assignment is performed by a scheduler node that also collects and stores the results in a data repository.

\metrichistory is an extensible tool designed to collect and process software measurements across multiple versions of a
code base. The measurement itself is modular and executed by a third party analyzer, in the case of this study we use
\sourcemeter. The core design principles of \metrichistory is to integrate into any toolchain using its command line
interface or Java API. It also aims to be easily customizable by its modular architecture. For example, adding a new
analyzer or supporting another \gls{vcs} only requires to implement an interface. \metrichistory is also operationally
modular to accommodate the explorative workflows of researchers. The results of each step mentioned in this chapter can
be saved or recomputed at any moment so one can resume from any point in their workflow and interchange data between
steps. In the case of the aforementioned distributed setting, each worker goes through all the steps at once for one
commit only. However, if you have a computer powerful enough or a smaller number of projects, it also support to
calculate all the measurements for one step for all commits in batch manner and save the results for a later use, or
analysis through a custom toolchain. A detailed description of \metrichistory's features, installation, and usage is
presented in Appendix~\ref{sec:appendix-metric-history}.

\section{Raw dataset (D)}
Since \metrichistory can make use of different analyzers -- each with their own output format -- we make a distinction
between the format of the metrics used to calculate the fluctuations and the output format of our analysis tool.
Supporting this transient step make our toolkit modular, allowing future use of, not only different analyzers, but also
pre-compiled measurements from other researchers and studies.

Using \metrichistory again, we convert the class measurements into a common basic "RAW" format. This scalable file
format identifies the measurements for a given artifact through multiple versions. This dataset contains the metrics
generated by \sourcemeter for each class (identified by its canonical name i.e. $com.example.FooBar$) for each version
obtained in step \textbf{A}. You will find below the 5 first lines of typical raw file. Only three metrics are shown for readability.

\begin{verbatim}
commit;class;LOC;CBO;DIT
9b23cc2184438f93923c972c45b7caeb43d77d24;org.animals.CowRenamed;19;2;1
9b23cc2184438f93923c972c45b7caeb43d77d24;org.animals.Dog;13;1;2
9b23cc2184438f93923c972c45b7caeb43d77d24;org.animals.Labrador;124;0;1
9b23cc2184438f93923c972c45b7caeb43d77d24;org.animals.Poodle;24;5;1
\end{verbatim}

For projects with a large number of commits to analyze, we can separate the results by commit. In other words, we create
one file for each commit instead of writing all the results into a single file for all commits and classes.

\section{Fluctuations dataset (E)}
Using the transformed data acquired in \textbf{D}, we use \metrichistory to compute the change of metric for each class
from each commit in \textbf{A}. For example, if class \emph{Foo}'s \textbf{LOC} metric is measured as 3 in commit 1 and
is measured at 5 in commit 2, the change of metric \emph{LOC} for \emph{Foo} in version 2 is of $5 - 3 = +2$. Commit 1
is the set of code changes directly before commit 2, also referred as the "parent" commit.

\section{Aggregation (F)}
The last step converts the metric fluctuations for each class (class fluctuations) into metric fluctuations describing
the changes in a commit (commit fluctuation). We aggregate the fluctuations of each metric across every changed class
into one value per metric using a naïve summation. For example, if commit 2 has classes \emph{Foo} and \emph{Bar} that
have changed with the metric fluctuations for metric \emph{LOC} as $+20$ and $+30$ respectively. Then the metric
fluctuation at the commit level will be $20 + 30 = +50$.

We note two crucial details. First, we only retain the metric fluctuations from classes that have
changed; we ignore classes that were added or deleted in order to capture design changes inside classes and not inside a
package or group of components. Second, we count the number of metrics that are affected by a change during the
aggregation process. This prevents metrics changes to disappear at the commit level (the sum is $0$) when metric
fluctuations at the class level are cancelling each other.

\section{Summary}
In this Chapter, we presented our pipeline and the datasets collected. Once the projects are identified, the first steps
are to determine the commits to include for each project (A) and then find the commits in this selection that contain
refactorings (B). Then, using \metrichistory, we extract the metrics at each commit for all projects to create the
native dataset (C). After that, we use \metrichistory to transform the native dataset in the raw dataset that contains
the metrics for each class across all commits (D). At that time, we create the fluctuation dataset that contains the
metric fluctuations for each class for all commits (E). Finally, we aggregate the class fluctuations with a summation
operator to create the metric fluctuations to represent individual commits (F).

% Notice that our analysis focuses on the changed classes of a revision, while
% added or deleted classes are ignored. The selection allows us to focus on the changes happening inside classes instead
% of at a larger scope.

% \section{Math}
% Let $\mathcal{V}_{all}$ be the set of all versions of a project and $\mathcal{V} \subset \mathcal{V}_{all}$ be the
% subset of revisions selected to be analyzed (i.e., commits to the default branch made before 2018-12-31, that were not
% merges). Let $\mathcal{A}_{all}$ be the set of all code artifacts in the project -- in our case, Java classes. Finally,
% let $\mathcal{M}_{all}$ be the set of metrics supported by the static analysis and $\mathcal{M} \subset
% \mathcal{M}_{all}$ the set of metrics selected for analysis.

% We define the result of the data collection step as a collection of records $x_{vam}$, each
% representing a value resulting of the combination of the metric $m \in \mathcal{M}$ for artifact $a \in
% \mathcal{A}_{all}$ at version $v \in \mathcal{V}$.

% In the following, we use a running example for illustration: consider a small system containing in version $v_1$ the
% classes \textit{Dog} and \textit{Cat} with the respective metrics [\textsc{wmc}: 5, \textsc{cbo}: 10] and
% [\textsc{wmc}: 4, \textsc{cbo}: 11]. For version $v_2$, the class \textit{Bird} is added and measurements changed to
% \textit{Dog}[\textsc{wmc}: 8, \textsc{cbo}: 3], \textit{Cat}[\textsc{wmc}: 2, \textsc{cbo}: 1],
% \textit{Bird}[\textsc{wmc}: 1, \textsc{cbo}: 7]. For example, $x_{vam}$ with $v = v_2$, $a = \text{'Bird'}$ and $m =
% \text{\textsc{cbo}}$ means $x_{vam} = 7$.


% The first step of this process
% calculates the metric fluctuations for each code artifact for each
% revision in $V$.

% A metric fluctuation is calculated by the function $f(x_1, x_2) = f(x_{vam}, x_{pam}) = x_{vam} - x_{pam} = d_{vam}$
% which calculates the fluctuation of metric $m$ between the version $v$ and $p$ ($v \in \mathcal{V}, p \in V_{all}$, $p$
% is older than $v$) of artifact $a$. Let $d_{vam}$ be the metric fluctuation. In
% our example, we
% calculate the fluctuation of the metric \textsc{wmc} in the class \textit{Dog} for the revision $v_2$ with $x_1 =
% (v_2,\textit{Dog}, \text{\textsc{wmc}}), x_2 = (v_1,\textit{Dog}, \text{\textsc{wmc}})$ which translates to $f(x_1, x_2)
% = f(8, 5) = 8 - 5 = +3$ thus $d_{vam} = +3$ with $v = v_2, a = \textit{Dog}, m = \text{\textsc{wmc}}$.

% This operation is repeated for the metrics of every artifact that changes in the revisions of $\mathcal{V}$. We define
% $\mathcal{A} \subseteq \mathcal{A}_{all}$ as the set of artifacts that strictly changes in a revision and that are not
% test classes. We define as test code any Java class whose name has the suffix ``Test''. This is a common Java naming
% convention and is followed in all of the projects in our study. In our example, $\mathcal{V}_{all} = \{v_1, v_2\}$ and
% $\mathcal{V} = \{v_2\}$. For $v_1$, $\mathcal{A}$ is undefined because we have no previous parent revision. For $v_2$,
% $\mathcal{A} = \{\textit{Dog}, \textit{Cat}\}$. The class \textit{Bird} is not included because in our analysis we do
% not consider created and deleted classes.
% %it didn't strictly change: it was created. Deleted classes are similarly excluded.

% The result of this step is a matrix  $D^{|\mathcal{A}| \times |\mathcal{M}|}_v$ where $|\mathcal{A}|,|\mathcal{M}| $
% represent the sizes of $\mathcal{A}$ and $\mathcal{M}$, respectively. This matrix represents the metric fluctuations for
% each changing artifact in revision $v$. In our example, we compute the matrix $D^{2\times2}_v, v = v_2$ in the
% previous step. It has 2 rows because $\mathcal{A} = \{\textit{Dog}, \textit{Cat}\}$ and 2 columns because we have
% $\mathcal{V} = \{\textsc{wmc}, \textsc{cbo}\}$. After applying $f(x_1, x_2)$ to each artifact, we obtain the (flat)
% matrix: $[+3, -7, -2, -10]$. Each value corresponds to an instance of $d_{vam}$ (e.g., $a = \textit{Cat}, m =
% \textsc{cbo}$ we obtain $-10$). $[+3, -7]$ are respectively the fluctuation of \textsc{wmc} and \textsc{cbo} for class
% \textit{Dog} and $[-2, -10]$ are the similar fluctuations for class \textit{Cat}.

% In summary, in this step, we introduce the operation $f(x_1, x_2)$ that computes the fluctuation of metric values
% between two versions. While our implementation of $f()$ uses real values on interval scale as input, this is not a
% limitation, and can be used with other scales or mathematical constructs (e.g., distributions).

% \begin{figure*}
%     \begin{center}
%     \includegraphics[width=0.8\textwidth]{figures/computation-process}
%     \end{center}
%     \caption{Illustration of the pipeline.}
%     \label{fig:computation-process}
% \end{figure*}

% We define the function $g(\vec{x}) = g(\vec{d}_{vm}) = g([d_{v1m}, d_{v2m}, ..., d_{v|A|m}]) = d_{v1m} + d_{v2m} + ... +
% d_{v|A|m} = r_{vm}$ to aggregate the fluctuations of metric $m$ across the artifacts $\mathcal{A}$. Thus, $r_{vm}$
% represents the trend of fluctuation of metric $m$ for revision $v$. For our
% analysis, we choose a naïve aggregation
% implemented as a sum. The symbol $\vec{x}$ represents a collection of values (i.e., a vector). Thus, $\vec{d}_{vm} =
% [d_{v1m}, d_{v2m}, ..., d_{v|A|m}]$ is the list of metric $m$ for every artifact in $\mathcal{A}$ for version $v$ which
% can be seen as a vertical slice of matrix $D_v$. In our example, $\vec{d}_{vm}$ with $v = v_2, m = \textsc{wmc}$ would
% be equal to $[+3, -2]$ because $+3$ is the value of \textsc{wmc} for class \textit{Dog} and $-2$ the value for class
% \textit{Cat}.

% We apply this function for each metric in $\mathcal{M}$ for version $v$ which yields us one $r_{vm}$ per metric. This
% collection of aggregations is expressed as $\vec{r}_{v} = [r_{v1}, r_{v2}, ..., r_{v|M|}]$. Thus, $\vec{r}_{v}$ contains
% the aggregated fluctuations for the metrics of $\mathcal{M}$ for revision $v$.  In our example, $r_{v1}$ corresponds to
% the result of the aggregation for fluctuations of metric \textsc{wmc} in classes \textit{Dog} and \textit{Cat} which is
% equal to $r_{v1} = (+3) + (-2) = +1$. We can also calculate $r_{v2} = (-7) + (-10) = -17$ for \textsc{cbo} and obtain
% our metric fluctuations at the revision level for revision $v_2$ as $\vec{r}_v = [+1, -17], v = v_2$.
