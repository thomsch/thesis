
\chapter{Codebook}
\label{sec:appendix-solid}
The annotation handbook presented below is copied as-is from its original document. The guidelines have been co-written
with Dr. Vassilis Zafeiris who authored most of the content. This version of the codebook was used as the guidelines for
the creation of the oracle presented in Chapter~\ref{sec:solid}.

\section{Annotation guidelines}
This work studies the recovery of design intent from the revision history of a software project. The focus is on the
identification of revisions whose change-set introduces structural improvements to system design. The recovery of design
intent from software revisions would contribute to better understanding the evolution of a project through
identification of important milestones in its lifecycle. Besides retrospection, the identification of design intent in
new commits could enhance the code review process by giving priority to those commits that alter design and could
potentially impact the architectural conformance of the software. We intend to create a classifier that characterizes
the presence or not of design intent to a given revision. 

The proposed method involves the following assumptions: 

\begin{itemize}
  \item Design intent denotes a deliberate attempt to improve system design
  \item Since the notion of design improvement is rather abstract and may refer to a broad set of design options, we
  proxy it with the intent for enforcement of well established OO design principles
  \item We adopt the SOLID set of design principles that focus on code maintainability
  \item The enforcement of a design principle denotes the introduction of structural changes to existing classes so as
  to achieve better conformance to the given principle, e.g. Single Responsibility Principle through relocation of
  functionality 
  \item Structural changes relevant to design improvement can be traced to the application of refactorings.
  \item The identification of structural changes in software revisions can be reduced to mining the application of
  refactorings. 
  \item Design interventions may, also (on purpose or not), deteriorate design, as manifested by the violation of design
  principles. Tracking of violations of design principles is currently out of context of this work, but could be an
  interesting part of another study. However, in these cases, refactorings cannot always reveal these violations. Most
  probably these could be traced to additions of methods/fields/dependencies to a given class.
\end{itemize}

Our method tries to identify the design intent on the basis of more primitive features of a revision: (a) the presence of refactorings and (a) the change in four object oriented metrics. Should we use the presence of each individual refactoring as a separate feature?

\subsection{Classifier for SOLID principles detection in RCs}
\begin{itemize}
  \item What if the presence of each principle is correlated with specific types of refactorings (e.g. SRP with extract
  method, OCP with extract interface/superclass etc.)? In this case metric fluctuations will have no effect on the
  classifier. Do we notice such a correlation in the results so far?
  \item Should revisions be evaluated by both reviewers and report on consensus on what is SOLID application or not?
  \item Could the analysis of non \glspl{rc} (as side result of our evaluation) strengthen our position?
  \begin{itemize}
    \item changed classes would probable involve addition/deletion of code which does not reflect design changes
    \item we may have enforcement of SOLID principles in new code (that is also taken into account during tagging
    \glspl{rc}). 
    \item presence of refactorings would be introduced as an extra feature
    [refactoring-revision: yes/no]
    \item requires evaluation of several revisions and we will be biased towards non-design. A better protocol could
    involve mixing refactoring and non-\glspl{rc} before manual tagging 
  \end{itemize}
\end{itemize} 

\subsection{Manual characterization of SOLID principles }
The manual analysis on the introduction of SOLID principles that was carried out for our initial submission to ICSE’18
is based on the following notes:

Notice that more than one principle may apply to a \gls{rc}. In any case, the application of at least one
denotes a design decision. The evaluation involves checking all the changes in the \glspl{rc} that the
identified refactoring is part of, for instance, if the refactoring is part of a feature implementation all relevant
changes are checked.

Document in generic terminology the intent of a design change with reference to SOLID principles, e.g.: An inner class
absorbs responsibility from its context class... 

\subsubsection{Single Responsibility Principle (SRP)}

The principle denotes that "each class, method, or module should have one reason to change". Simple heuristics on the
application of SRP

\begin{itemize}
  \item move functionality of a class to another class (Move Method) to increase its cohesion, considered in every case of
  functionality redistribution among classes, e.g., move of a responsibility from a subclass to its parent, 
  \item considered also in cases of attribute relocation among classes, 
  \item  although the resulting classes usually do not have a single responsibility, any contribution towards a better situation
  is tagged with SRP 
  \item  replace constructor with factory method (a class acquires stricter control over the creation of its objects)
  \item  Extracting functionality or behavior to a new method 
  \item  replacement of constants with enumerations (single responsibility for the domain of value types?)
\end{itemize}

\subsubsection{Open Closed Principle (OCP)}
The principle states that "a class should be open for extensions and closed for changes". Simple heuristics on the
application of OCP:
\begin{itemize}
  \item refactorings or changes that implement Template Method or Strategy Design patterns 
  \item a class dependency (constructor parameter, method parameter or injected in other way) is replaced by an abstract
  class or interface
  \item a class is decoupled from a concrete implementation of a specific abstraction.
  \item replace conditional logic with polymorphism
\end{itemize}

\subsubsection{Liskov's Substitution Principle (LSP)}
The principle states that "supertype objects can be replaced by subtype objects without breaking the program". The
principles is considered to be applied in cases of refactoring/changing members of an inheritance hierarchy: 

\begin{itemize}
  \item the public interface of a child class is narrowed down to conform to that of its parent (not found in any case in
  \jfreechart/\retrofit) 
  \item method implementation changed for contract rules enforcement (hard to find, not found in any case),
  i.e.
  \begin{itemize}
    \item method preconditions should not be strengthened
    \item postconditions should not be weakened
    \item invariants of the supertype must be maintained in the subtype 
  \end{itemize}

  \item enforcement of variance rules (parameter contravariance, return type covariance) through class parameterization
  (generic class)
  \begin{itemize}
    \item e.g. a parent class that uses Object, Map, List etc. method parameters or return types is converted to
    parametric class and respective types replaced by class parameters. 
    \item the subclasses usually remove relevant type checking code. 
  \end{itemize}
  \item some cases found in \retrofit although not characterized by changes in revision metrics
  \item Implementation of multiple classes to implement different interfaces, such as multiple ActionListener.
  \item  replace inheritance with composition and vice versa
\end{itemize}


\subsubsection{Interface Segregation Principle (ISP)}
The principle states that "clients should not be forced to depend on methods they do not use", in other words a class
should implement small interfaces with targeted functionality.

The application of the principle is spotted when 
\begin{itemize}
  \item a fat interface is split into two or more simpler interfaces (found in retrofit)
\end{itemize}

\subsubsection{Dependency Inversion Principle (DIP)}
The principle states that "high-level modules should not depend on low level modules - both should depend on
abstractions.'' According to R.Martin "a naïve still powerful interpretation of DIP: depend on abstractions", i.e. no
variable should hold reference to concrete class, no class should derive from concrete class, no method should override
an implemented method (and call the super implementation).

Simple heuristics on the application of DIP are: 
\begin{itemize}
  \item replacement of implementation inheritance to interface inheritance (remove overriding of concrete methods), although
  not found in \jfreechart/\retrofit 
  \item in \retrofit, cases of refactoring/changing code to depend on abstractions is tagged as OCP 
  \item search for "ownership inversion: clients own the abstract interfaces and servers derive from them (Hollywood Principle)"
  \item the implementations of an interface are distributed in a separate module, e.g. \retrofit main module owns
  Converter, Adapter etc. interfaces and knows nothing about implementations   that are provided in separate modules
\end{itemize}

% \subsection{Package design principles (package cohesion)}
% Although not belonging typically to the set of SOLID principles, R.Martin refers to them. Their application is
% considered in case of code rearrangement through moving of classes among packages or source folders.

% \subsubsection{Reuse/Release Equivalence Principle (REP)}
% classes are grouped in a component (project module build independently) in order to be releasable together

%  e.g., experimental code is integrated into the main module, two or modules are consolidated

% \subsubsection{Common Reuse Principle (CRP)}
% "users of a component should not be forced to depend on things they don't need"
% split a component to components with different dependencies or concerns, e.g., distribute Android related functionality in retrofit to a separate module
% at a class level I tagged as CRP cases of Move Class as inner classes into another, in the sense that they will always reused together

% \subsubsection{Common Closure Principle (CCP)}
% "SRP restated for components", gather into components classes that change for the same  reason and at the same time, or separate them if they change for different reasons/time
% split a component into cohesive components with specific responsibilities
% move classes to components with relevant responsibility, e.g. move the HTTP body converter in retrofit from the main module to the module corresponding to respective body format (e.g. GSON converter)

% The above principles are not part of SOLID set of principles. Could be removed.
% In addition to Package Cohesion principles there are Package Coupling principles that
% are more difficult to evaluate and were not considered:
% - Stable Dependencies Principle
% - Stable Abstractions Principle
% - Acyclic Dependencies Principle

% \section{Annotation Template}
% \todo{Describe each column of the GSheet and copy the instructions}

% \section{User Study Template}
% \todo{Describe each column of the GSheet and copy the instructions}