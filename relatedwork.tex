\chapter{Related work}
\label{sec:relatedwork}
% \todo{Reread and minor rewriting}
% \todo{Add related work from the articles I've read in Notion and the Google Sheet}

Metrics have played an important role in evaluating the quality of a software system and in guiding its design and
evolution. This role is summarized very well by Stroggylos et al.~\cite{stroggylos07}, who present a set of research
works that have used metrics for these tasks especially in the context of refactoring. The authors also present a study
where, similarly to our work, they measure software quality metrics before and after refactorings for a set of
object-oriented systems. According to their findings, the impact on metrics depends on the subject system, the type of
refactoring and in some cases on the tool used to measure the metrics. Nevertheless, the impact is not always positive,
as one would expect. This has motivated our study and definition of tradeoffs, in order to correlate metrics and
refactoring activity with design intent that possibly justifies any potential deterioration in quality metrics. In our
study, we automated the refactoring detection, allowing us to measure the effect on the design of more projects. We were
able to confirm that refactoring activities does not necessarily lead to improvements in quality in 13 projects compared
to their four original projects. Additionally, we provide an intuitive and scalable characterization of commits by
classifying them in scenarios and then in a refined taxonomy.

Each type of refactoring may affect multiple metrics and not always in the same direction. Researchers have explored
this complex impact to detect design problems known as code smells. Marinescu defined thresholds on a number of metrics
and then combined those using AND/OR operators in rules called detection strategies~\cite{marinescu04}. A detection
strategy could identify an instance of a design anomaly, which could orthogonally be fixed by a corresponding
refactoring. A very similar approach, using metrics and thresholds, was followed by Munro et al.~\cite{munro05}.
Although, in principle, metric tradeoffs could be captured in detection rules, Tsantalis et al. went one step further
and defined a new metric to capture a trade-off~\cite{tsantalis09}. Since, coupling and cohesion metrics can often be
impacted in opposite directions during refactoring~\cite{stroggylos07}, Tsantalis and Chatzigeorgiou defined a new
metric, \emph{Entity Placement}, that combines coupling and cohesion. Quality assessment using Entity Placement is
supposed to give more global results with respect to detection and improvement after refactoring. K\'ad\'ar et al. and
Heged{\"u}s et al. focused exclusively on the relation between metrics and maintainability in-between
releases~\cite{kadar16, Hegeds2018EmpiricalEO}. A cyclic relation was found, where low maintainability leads to extended
refactoring activity, which in turn increases the quality of the system.

The activity of refactoring and its relation to design has been extensively studied. Ch\'{a}vez et al. performed a
large-scale study to understand how refactoring affects internal quality attributes at a metric level~\cite{chavez17}.
In contrast, our study aims at exploring the specific role of refactoring on internal qualities when metrics embody a
tradeoff and how it relates to design. Cedrim et al. investigated the extent to which developers are successful at
removing code smells while refactoring~\cite{cedrim16}. Soetens et al. analyzed the effects of refactorings on the
code's complexity~\cite{soetens2010}. Tsantalis et al. investigated refactorings across 3 projects and examined the
relationship between refactoring activity, test code and release time~\cite{tsantalis13}. They found that refactoring
activity is increased before release and it's mostly targeted at resolving code smells. Compared to the study presented
in this paper, their study was not guided by metrics, nor did it involve extensive qualitative analysis and, finally, it
did not discuss more major design decisions as part of the intent.

Recovery of design decisions has been studied from an architectural perspective. Jansen et al. proposed a methodology
for recovering architectural design decisions across releases of a software system~\cite{Jansen08}. The methodology
provides a systematic procedure for keeping up-to-date the architecture documentation and prescribes the steps that the
software architect must follow in order to identify and document architectural design decisions across releases. A fully
automated technique for the recovery of architectural design decisions has been, recently, proposed by Shahbazian et
al.\cite{shahbazian2018}. The technique extracts architectural changes from the version history of a project and maps
them to relevant issues from the project's issue tracker. Each disconnected subgraph of the resulting graph corresponds
to an architectural decision. The recovered decisions are relevant to structural changes in system components, applied
across successive releases.Our method focuses on decisions affecting detailed design and concern the structure of
classes and the distribution of state and behavior among them.

Moreover, decision recovery takes place at the revision level and is guided by metrics' fluctuations and fine-grained
changes due to refactorings. Besides, we employ issue tracker information for manual cross-checking of design decisions
as well as commit messages and source code comments. Their big picture is very similar to ours. However, there is
considerable differences in the approaches. Our main goal is to find design tradeoffs. In our study, the tradeoffs
emerge from the metric fluctuations between versions. The design detection is a component of our process that is done
manually. Another big difference with their study is the size. They analyze fewer version than us but the projects they
choose have a high density of issues that can be mapped to commits. Our method focuses on decisions affecting the lower
level design and which concern the structure of classes and the distribution of state and behavior among them.
