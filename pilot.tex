\chapter{Exploratory studies}
\label{sec:preliminary}

In this section, we are interested to lay the basis of knowledge regarding metric fluctuations and determine if our
hypothesis presented in Chapter~\ref{sec:introduction} is sound. We hypothesized that when a \gls{rc} affects multiple
metrics, improving some while deteriorating others, we have evidence of a developer intentionally changing the design
(design intent) by making specific tradeoffs. Specifically, we explore the concept of metric fluctuations and their
relation to development activities in the context of design. We conduct three experiments from the datasets created
through the approach presented in Chapter~\ref{sec:datacol}.

The first experiment is a qualitative study on a subset of commits of \jfreechart where we classify revisions into four
categories based on the pattern of the metric fluctuations. We focus on \glspl{rc} because they are likely to contain
intentional structural changes from developers. We then manually analyze each commit to understand its changes and their
relation to the categories. In the second experiment, we conduct a quantitative study where we analyze all the commits
of the 13 projects under different lenses to test the generalizability of metric fluctuations. Finally, in a third
experiment, we look at the capacity of metric fluctuations to act as indicators for design related changes,
specifically design intent, in a sample of commits.

In the subsequent explorations, to detect and characterize changes in the software’s design, we measure its internal
quality properties using multiple metrics. We focus our analysis on four metrics on the interval scale: \gls{cbo} to
measure coupling, \gls{dit} for inheritance complexity, \gls{lcom5} for cohesion, and \gls{wmc} for method complexity.
We focus on this small set of metrics as they are considered some of the most representative for their particular
properties and good indicators of design quality~\cite{marinescu04,stroggylos07}. In addition, to locate intentional
changes, we scope our exploration using refactorings as they are embodying conscious changes made by
developers~\cite{Silva16}. Finally, when coupled with our aforementioned hypothesis, it means that commits containing
tradeoffs expressed by metric fluctuations have a strong likelihood to be indicators of the implementation of design
decisions made by the developers.

By characterizing the metric fluctuations across the history of development, we aim to provide a baseline for
understanding changes to identify development activities, and facilitate the detection of specific change patterns that
can be relevant as documentation to developers or other stakeholders.

\input{explore-jfreechart}
\input{explore-tradeoffs}
\input{explore-designintent}

\section{Threats to validity}
\label{sec:threats-validity}
\textbf{Construct validity} is threatened by multiple sources. First, by using only four metrics, we are bound to miss
some aspects of changes in a \gls{rc}. To mitigate this, we selected metrics that have been tied to well known internal
quality attributes and that are general enough to capture a maximum of their respective code aspect.

Second, as mentioned in Chapter~\ref{sec:datacol}, we ignore the metric fluctuations generated from classes that are
added or deleted. This means our pipeline does not capture metric fluctuations on the entire change set of a revision,
in cases of class addition or removal. Interestingly, we observed no statistical differences in distributions for
detecting tradeoffs when taking into account added and deleted classes.

Third, we concentrated on metric fluctuations at the class level. Doing that, we might have missed variations in metrics
in smaller or larger granularities. To mitigate this, we count the number of metric changed when we aggregate the
metrics at the class level to represent the revision. This way, we would see if a metric changed even if the changes of
metric of two classes or more would cancel each other. For these reasons, we cannot draw general conclusions about what
would happen at different granularity levels or with other metrics. Regardless, this does not impact the main
contributions of this exploratory, i.e., the systematized methodology, classification scheme and research questions.

% Additionally, our process of aggregation -- summing up the metric's value for each changing component -- to represent
% the change in one revision is not perfect. After all, this is the definition of what a model is. Specifically, when
% summing up the metrics it can happen that component A has an increase of metric $m_1$ of $+2$, and simultaneously
% component B has a decrease of $-2$ for the same metric. The resulting value for the revision would be $0$, indicating
% no change. We mitigate this threat by counting the number of metrics that increase or decrease. This way, we can
% register metric $m_1$ with a neutral change. Regardless, this does not impact the main contributions of these
% explorative studies since we do not claim generalization. This granularity does not necessarily allow drawing general
% conclusions at the project level.

Fourth, we do not count \refact{Rename} operations as design decisions in Section~\ref{sec:explore-jfreechart}. One
could argue that pure \refact{Rename} refactorings (who constitutes the majority of scenario 1) also defines design
decisions. After all, we use natural language to communicate intent. If the name of an entity change so should the
intent behind the class. However, for this study we are only interested in structural or architectural design decisions.

Fifth, the initial inter-rater agreement presented in Section~\ref{sec:explore-manual-eval} is also a potential threat
to validity since the reviewers were only in \textit{moderate} agreement. This threat was mitigated by the conflict
resolution procedure we conducted afterward to harmonize the datasets. Moreover, from this experience, we were able to
improve our manual analysis procedures for subsequent usages such as Section~\ref{sec:oracle}.

Finally, the study is based on the analysis of arbitrarily-cohesive units of work determined by each developer. The
content of a commit is determined by the developer. During our manual analysis, we saw different level of commit hygiene
-- how well a commit is made. Does it contain a good description? Are the changes separated? Some would neatly describe
the changes and include strictly the relevant code changes. Others, would commit everything they changed since their
last commit whether it was a few hours or a few weeks, resulting in very hard to read commits~\cite{arima2018}.
Incidentally these commits tended to also have the worst descriptions. In our context, this diversity is not very
relevant since we're doing exploration. However, while building a classifier it could become a problem and cause the
model to miss classify commits. However, this is mitigated by the fact that big commits are likely to touch multiple
concerns of the code and thus certainly affect design that will be reflected in the metrics. In a more fine grained
approach where we want to locate the design changes, this would potentially be a problem. Approaches are being
researched to untangled code changes in commits~\cite{Sothornprapakorn18}.

\textbf{Internal validity} is threatened by the off-the-shelf tools used in our data processing pipeline. Despite its
high accuracy and recall, \rminer has a small chance to produce false positives and miss refactorings. To mitigate this
factor, we combed through the refactorings of \jfreechart manually to remove false positives and assessed the gravity of
the threat. We determined that \rminer was giving false positives in cases that were innocuous to the study. Allowing us
to proceed for the other projects with confidence.

Additionally, we depend on \sourcemeter for the calculation of metrics and are, therefore, tied to its quality. This
threat will be mitigated in future reproductions of the study, since we architected our toolchain such that new versions
of the tool or entirely different tools can be easily integrated.

\textbf{External validity} is threatened by the generalizability of the study. We analyzed 13 open source projects and
focused on a subset of their version history. Thus, our study is biased by the development practices used in these
projects. An argument towards the representativeness of the selected sample of \glspl{rc} are the different levels of
commit hygiene revealed by a preliminary manual analysis; some revisions were very well documented and worked toward a
clear, unique, defined goal while some other had misleading, vague or empty descriptions with code changes affecting
different concerns (tangled commits). However, our studies are exploratory in nature, with a clearly defined scope; we,
therefore, do not claim that our results are generalizable, but rather make an existential argument that it is possible
to detect design tradeoffs using refactorings as an indicator.

Another concern is the reproducibility of our study. To ensure that our findings are reproducible, we explicitly
documented each step of our study, using existing, publicly available data and tools and published our custom-developed
tool \metrichistory and associated scripts as open source.

\section{Lessons Learned}
In this Chapter, we presented groundwork to validate our initial assumption and answer the \glspl{rq} 1, 2, and
3 presented in Section~\ref{sec:research-questions}.

In Section~\ref{sec:explore-jfreechart}, we explored the relationship between design, internal quality metrics, and
\glspl{rc} in the project \jfreechart. We found that \glspl{rc} can be classified into four scenarios. Each scenario
captures a specific pattern of changes. Moreover, we found that scenarios containing tradeoffs between different quality
metrics were more susceptible to contain design related changes compared to the first and second scenario, validating our
assumption and sketching the beginning of the answers to \glspl{rq} 1, 2, and 3.

In Section~\ref{sec:explore-tradeoffs}, we conducted a large scale quantitative study on thirteen projects. We refine
our classification into a two-dimensional taxonomy, providing an answer for \gls{rq}2. Then we observe the metric
fluctuations in various development contexts. We observed a noticeable effect on metric fluctuations depending on the
environmental context answering \gls{rq}3. Additionally, we observed that about half of the tradeoffs between coupling,
complexity, inheritance, and cohesion captured in our approach appear in commits containing \gls{rc}. This finding,
connected with the results of the study presented in Section~\ref{sec:explore-jfreechart} provides further evidence
towards \gls{rq}1. The conjunction demonstrates that refactorings contain valuable design changes and that metric
fluctuations, particularly tradeoffs, have a certain correlation with design related changes.

Finally, we conduct a third study in Section~\ref{sec:prelim-design-intent} where we conduct a qualitative analysis over
a sample of the projects to understand the extent to which tradeoffs are indicators of design related changes and intent
in the context of refactoring. The results show that a measurable correlation exists, thus completing our answer to
\gls{rq}1 and provides a compelling foundation for the study presented in Section~\ref{sec:solid}.
